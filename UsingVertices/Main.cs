using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

using GameLibrary;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;

namespace UsingVertices
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        public CameraManager cameraManager;
        public KeyBoardManager keyboardManager;
        public MouseManager mouseManager;
        public ObjectManager objectManager;
        public ObjectiveManager objectiveManager;
        public BillBoardManager billBoardManager;

        public bool MissionSuccess = false;

        public static bool startGame = false;
        public int simpleMenuIndex = 0;
        public Texture2D[] simpleMenu;

        public Song music;
    


        public float delta;
        public Integer2 viewportCentre;
        public BasicEffect wireframeEffect, textureEffect;

        public AirPlane plane;
     
        public TrackManager trackManager;
        public SpriteFont font;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
            
           
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            music = Content.Load<Song>(@"Assets\power");
            //record centre point for the screen
            viewportCentre = new Integer2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);
            DebugShapeRenderer.Initialize(GraphicsDevice);
            InitialiseStaticPointers();
            InitialiseManagers(); 
            InitialiseEffect();
            InitialiseCoordAxis();
            InitialiseObjects();
            InitialiseTerrain();
            //InitialiseModels();
            InitialiseCameras();
            font = Content.Load<SpriteFont>(@"Assets\Fonts\debugfont");
            simpleMenu = new Texture2D[2];
            simpleMenu[0] = Content.Load<Texture2D>(@"Assets\Textures\Controls");
            simpleMenu[1] = Content.Load<Texture2D>(@"Assets\Textures\gameInfo");
            MediaPlayer.Volume = 0.5f;
            MediaPlayer.Play(music);
            MediaPlayer.IsRepeating = true;
          
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        private void InitialiseStaticPointers()
        {
            //all objects drawn on screen have a static pointer to the Main game class
            Object3D.game = this;
            Objective.game = this;
            BillBoard.game = this;
            ObjectiveObject.game = this;
            HelperClass.game = this;
            MiscAssets.load(Content);
        }
        private void InitialiseEffect()
        {
            //initialise the BasicEffect used by ALL things that we draw
            wireframeEffect = new BasicEffect(GraphicsDevice);
            //enable color for these vertices
            wireframeEffect.VertexColorEnabled = true;

            textureEffect = new BasicEffect(GraphicsDevice);
            textureEffect.TextureEnabled = true;
            textureEffect.VertexColorEnabled = true;
        }


        private void InitialiseCoordAxis()
        {
          // objectManager.Add(new WireframeBuddy("origin",  Color.White, Color.White));
        }

        private void InitialiseTerrain()
        {
            Terrain theTerrain = new Terrain("terrain",
             Matrix.Identity * Matrix.CreateScale(9000f, 0f, 9000f) * Matrix.CreateTranslation(-9000f, 0f, 9000f),
                 Content.Load<Texture2D>(@"Assets\Textures\Terrain\heightmap"),
                 Content.Load<Texture2D>(@"Assets\Textures\Terrain\Grass"),
                     Vector3.One * 2);

            //objectManager.Add(theTerrain);
        }

        private void InitialiseModels()
        {
            Model torus = Content.Load<Model>(@"Assets\Models\torus");
            Model hedra = Content.Load<Model>(@"Assets\Models\hedra");
            Model cylinder = Content.Load<Model>(@"Assets\Models\cylinder");

            Texture2D texture1 = Content.Load<Texture2D>(@"Assets\Textures\Props\Crates\avatartexture");

            objectManager.Add(new BasicModel("torus", new Vector3(0.05f, 0.05f, 0.05f), new Vector3(0, 0, 0), new Vector3(50, 4, 0),
             torus, texture1));

            objectManager.Add(new BasicModel("hedra", new Vector3(0.5f,0.5f,0.5f), new Vector3(0, 0, 0), new Vector3(75, 4, 0),
             hedra, texture1));

            objectManager.Add(new BasicModel("torus", new Vector3(0.05f, 0.05f, 0.05f), new Vector3(0, 0, 0), new Vector3(100, 4, 0),
               torus, texture1));
        }

        private void InitialiseObjects()
        {
            
            #region SKYBOX
            Texture2D skyBox_Left = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Skybox\skybox_Left");
            Texture2D skyBox_Right = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Skybox\skybox_Right");
            Texture2D skyBox_Front = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Skybox\skybox_Front");
            Texture2D skyBox_Back = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Skybox\skybox_Back");
            Texture2D skyBox_roof = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Skybox\skybox_roof");
            Texture2D skyBox_Bottom = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Skybox\skybox_Bottom");
       
            float skyboxScale = 10000f;
            Matrix scale = Matrix.CreateScale(skyboxScale);
            TextureQuad skyboxLeft = new TextureQuad("skyboxLeft", scale*Matrix.CreateRotationY(MathHelper.ToRadians(90))*
                Matrix.CreateTranslation(new Vector3(-skyboxScale, 0, 0)), Color.White, 1, skyBox_Left);

            TextureQuad skyboxRight = new TextureQuad("skyboxRight", scale * Matrix.CreateRotationY(MathHelper.ToRadians(-90)) *
                Matrix.CreateTranslation(new Vector3(skyboxScale, 0, 0)), Color.White, 1, skyBox_Right);

            TextureQuad skyboxFront = new TextureQuad("skyboxFront", scale *
                Matrix.CreateTranslation(new Vector3(0, 0, -skyboxScale)), Color.White, 1, skyBox_Front);

            TextureQuad skyboxBack = new TextureQuad("skyboxBack", scale * Matrix.CreateRotationY(MathHelper.ToRadians(180)) *
                Matrix.CreateTranslation(new Vector3(0, 0, skyboxScale)), Color.White, 1, skyBox_Back);

            TextureQuad skyboxRoof = new TextureQuad("skyboxRoof", scale * Matrix.CreateRotationX(MathHelper.ToRadians(90)) *
               Matrix.CreateTranslation(new Vector3(0, skyboxScale, 0)), Color.White, 1, skyBox_roof);

            TextureQuad skyboxBottom = new TextureQuad("skyboxBottom", scale * Matrix.CreateRotationX(MathHelper.ToRadians(-90)) *
               Matrix.CreateTranslation(new Vector3(0, -skyboxScale, 0)), Color.White, 1, skyBox_Bottom);

            #endregion
            Texture2D missileGlow = Content.Load<Texture2D>(@"Assets\Textures\Glow\glow2");
            Missile.texture = missileGlow;
            Missile.initialise(GraphicsDevice);
     
            TexturedCubeParams.Initialise(this.GraphicsDevice, new Vector2(1,1));

      
            PlaneParams.initialise(GraphicsDevice);
         
          //  TextureQuad leftWall = new TextureQuad("LeftSkybox", Matrix.Identity, Color.White, 1, skyBoxTex);
            SpriteFont spriteFont = Content.Load<SpriteFont>(@"Assets\Fonts\debugfont");
            Components.Add(new GamePerformance(this, spriteFont));


            ObjectiveArrowParams.initialise(GraphicsDevice,this.Content);
            createLevel();
            

        }
        private void createLevel()
        {
            Vector3 playerSpawn = Vector3.Zero;

            #region HOMEBASE
            Texture2D playerText = Content.Load<Texture2D>(@"Assets\Textures\SpaceShip");
            ArmyBase base1 = new ArmyBase(new Vector3(-1000f, 3,-500f ),5,5);
            ObjectiveHome homeBase = new ObjectiveHome(base1.position + new Vector3(800, -25f, 100),Vector3.One * 30f, MiscAssets.heliPad);
            plane = new AirPlane(playerText, Vector3.One, homeBase.POSITION + new Vector3(0,5f,0));
            plane.POSITION = homeBase.POSITION + new Vector3(0, 33f, 0);
            
            #endregion
            #region random Bases
            ArmyBase base2 = new ArmyBase(new Vector3(-400f, 3, 0), 2, 1);
            ArmyBase base3= new ArmyBase(new Vector3(-400f, 3, -300f), 3, 2);
            ArmyBase base4 = new ArmyBase(new Vector3(-100f, 3, -100f), 2, 1);
            ArmyBase base5 = new ArmyBase(new Vector3(-300f, 3, -300f), 1, 2);
            City city1 = new City(new Vector3(-200f, 2,-1300f), 3, 3);
            #endregion
            #region OBJECTIVES

            List<ObjectiveObject> tempList = new List<ObjectiveObject>();
            List<ObjectiveObject> enemyList = new List<ObjectiveObject>();
            List<ObjectiveObject> enemyList2 = new List<ObjectiveObject>();
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                int temp2 = rand.Next(5, 60);
                tempList.Add(new ObjectivePickup(new Vector3(1200 + i * temp2, temp2, -2000 + i * temp2), new Vector3(10, 10, 10), MiscAssets.cargoTexture));
            }
            for (int i = 0; i < 2; i++)
            {
                enemyList.Add(new EnemyShip("Grunt" + i, new Vector3(3f, 3f, 3f), new Vector3(i * 800, i*200, 1900)));
            }
            for (int i = 0; i < 5; i++)
            {
                enemyList2.Add(new EnemyShip("Grunt" + i, new Vector3(3f, 3f, 3f), new Vector3(i * 800, i * 200, 1900)));
            }

            Objective cargo1 = new Objective("Collect ShipMent", Vector3.Zero, 22,
                new ObjectivePickup(base3.position + new Vector3(-140, 10, 250), new Vector3(2, 2, 2), MiscAssets.cargoTexture));
            Objective cargo3 = new Objective("Collect ShipMent", Vector3.Zero, 35,
              new ObjectivePickup(base3.position + new Vector3(2800, 290, 2800), new Vector3(6, 6, 6), MiscAssets.cargoTexture));
            Objective cargo2 = new Objective("Cargo Pickup", Vector3.Zero,60, tempList);
           Objective enemies = new Objective("Kill Enemies", Vector3.Zero, 0, enemyList);
            Objective enemies2 = new Objective("Kill Enemies", Vector3.Zero, 0, enemyList2);
            Objective goHome = new Objective("Return To Base", Vector3.Zero, 0, homeBase);
            #endregion

           
            City city = new City(new Vector3(2000f, 2, 2000f), 30, 10);
          //  City city2 = new City(new Vector3(-1000f, 2, 2000f), 10, 10);

        }
        private void InitialiseManagers()
        {
            //create and add keyboard manager  - this represents an object that gives access to the keyboard
            this.keyboardManager = new KeyBoardManager(this);
            Components.Add(keyboardManager);
            Services.AddService(typeof(KeyBoardManager), keyboardManager);

            //create and add keyboard manager  - this represents an object that gives access to the keyboard
            this.mouseManager = new MouseManager(this);
            Components.Add(mouseManager);
            Services.AddService(typeof(MouseManager), mouseManager);

            //create and add camera manager  - this stores a list of all camera objects
            this.cameraManager = new CameraManager(this, 1);
            Components.Add(cameraManager);
            Services.AddService(typeof(CameraManager), cameraManager);

            //create and add model manager - this stores a list of all drawn objects
            this.objectManager = new ObjectManager(this);
            Components.Add(objectManager);
            Services.AddService(typeof(ObjectManager), objectManager);

            this.billBoardManager = new BillBoardManager(this);
            Components.Add(billBoardManager);
            Services.AddService(typeof(BillBoardManager), billBoardManager);

            //adds a storage container that stores camera tracks
            this.trackManager = new TrackManager();
            Services.AddService(typeof(TrackManager), trackManager);

            this.objectiveManager = new ObjectiveManager(this);
            Components.Add(objectiveManager);
            Services.AddService(typeof(ObjectiveManager), objectiveManager);
        }

        private void InitialiseCameras()
        {
            //all cameras need access to main
            Camera.game = this;

            //add flight camera

           
            if (plane != null)
            {
                FlightCamera flightCam = new FlightCamera(plane);
                flightCam.Reset();
                //new camera!!!
                cameraManager.Add(
                    flightCam);
            }
          //  cameraManager.Add(new FirstPersonCamera(new Vector3(0, 0, 0), new Vector3(0, 0, -1), Vector3.Up));
            //add a track camera to track the moveable cube
       
            //add rail camera 1
             cameraManager.Add(new RailCamera(RailCameraPathSettings.getSlidePath(CurveLoopType.Oscillate), Vector3.UnitY));

            //add rail camera 2
             cameraManager.Add(new RailCamera(RailCameraPathSettings.getVerticalDropPath(CurveLoopType.Oscillate), Vector3.UnitZ));
             RepairShop pick = new RepairShop(plane.POSITION + new Vector3(-1400f, -8f, 0));

        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (keyboardManager.isKeyPressed(Keys.Escape))
            {
                this.Exit();
            }
            if (AirPlane.gotHome)
            {
                if (plane.objectivesCompleted > plane.objectivesFailed)
                {
                    MissionSuccess = true;
                }
                else
                {
                    MissionSuccess = false;
                }
            }
            if (startGame)
            {
                // Allows the game to exit
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                    this.Exit();
                if (keyboardManager.isKeyPressed(Keys.F))
                {
                    this.graphics.ToggleFullScreen();
                }
                this.delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

                Window.Title = cameraManager.ActiveCameraType + ", Position: " + cameraManager.ActiveCamera.Position.ToString();

                //cycle between cameras in the manager
                if (keyboardManager.isKeyPressed(Keys.C))
                    cameraManager.Cycle();

                rayIntersection();

                //if (!ObjectiveManager.allComplete)
                //{

                base.Update(gameTime);
            }
            else
            {
                if (keyboardManager.isKeyPressed(Keys.Enter))
                {
                     simpleMenuIndex++;
                     if (simpleMenuIndex > simpleMenu.Length-1)
                     {
                         startGame = true;
                     }

                  
                }
                keyboardManager.Update(gameTime);
            }
         //   }
        }

        //basic ray picking using bounding spheres
        private void rayIntersection()
        {
            //was button clicked?
         //   if (mouseManager.isLeftButtonPressed())
          //  {
                //iterate through all drawn objects
            Ray mouseRay = cameraManager.ActiveCamera.GetPickRay(mouseManager.Position, graphics.GraphicsDevice.Viewport);
                 for (int i = 0; i < objectManager.Length; i++)
                {
                     //read each object out from object manager
                    Object3D obj = objectManager[i];

                     //test if its a pickable object (i.e. an object with bounding sphere defined)
               
                        //cast a ray from mouse position to far clipping plane
                      

                            //scale the bounding sphere of the mesh because by default it will NOT have the world transform for the mesh applied
                        BoundingSphere boundingSphere = obj.boundingSphere;
                            //are we intersecting?
                        obj.rayPicked = false;
                            if (mouseRay.Intersects(boundingSphere) != null) 
                                //or ContainmentType.Intersects
                            {
                                if (obj is AirPlane == false)
                                {
                                    obj.rayPicked = true;
                                }
                                
                               
                            }
                        
                    
                }
                // objectiveManager.currentObjective.objective.rayPicked = false;
                 //if (mouseRay.Intersects(objectiveManager.currentObjective.objective.boundingSphere) != null)
                 //{
                 //    objectiveManager.currentObjective.objective.rayPicked = true;
                 //}


           // }
        }



        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            if (startGame)
            {
                //anytime the view of projection matrix we need to update both these attributes
                wireframeEffect.View = cameraManager.ActiveCamera.View;
                wireframeEffect.Projection = cameraManager.ActiveCamera.Projection;

                GraphicsDevice.RasterizerState = RasterizerState.CullNone;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default;

                // textureEffect.LightingEnabled = true;
                textureEffect.View = cameraManager.ActiveCamera.View;
                textureEffect.Projection = cameraManager.ActiveCamera.Projection;


                base.Draw(gameTime);
            }
            else
            {
                spriteBatch.Begin();
                spriteBatch.Draw(simpleMenu[simpleMenuIndex], new Rectangle(0, 0, GraphicsDevice.Viewport.Width,
                   GraphicsDevice.Viewport.Height), Color.White);

                spriteBatch.End();
            }
            if (AirPlane.gotHome)
            {
                if (MissionSuccess)
                {
                    String text = "Mission Success";
                    Vector2 textSize = font.MeasureString(text);
                    spriteBatch.Begin();
                    spriteBatch.DrawString(font, text, new Vector2(viewportCentre.X - textSize.X / 2, viewportCentre.Y + textSize.Y / 2), Color.White);
                    spriteBatch.End();

                }
                else if (!MissionSuccess)
                {
                    String text = "Mission Failed";
                    Vector2 textSize = font.MeasureString(text);
                    spriteBatch.Begin();
                    spriteBatch.DrawString(font, text, new Vector2(viewportCentre.X - textSize.X / 2, viewportCentre.Y + textSize.Y / 2), Color.White);
                    spriteBatch.End();

                }
            }
         //   spriteBatch.Begin();
         //   spriteBatch.DrawString(font, "Objects : " + objectManager.Count, Vector2.Zero, Color.White);
         //   //spriteBatch.DrawString(font, "Cameras : " + cameraManager.Count, new Vector2(0, 20), Color.White);
         //   //spriteBatch.DrawString(font, "Objectives : " + objectiveManager.Count, new Vector2(0, 40), Color.White);
         //   spriteBatch.End();
         ////   DebugShapeRenderer.Draw(gameTime, cameraManager.ActiveCamera.View, cameraManager.ActiveCamera.Projection);
        }
    }
}
