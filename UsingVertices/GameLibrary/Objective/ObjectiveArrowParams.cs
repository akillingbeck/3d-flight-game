﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace GameLibrary
{
    public class ObjectiveArrowParams 
    {
        public static VertexPositionColorTexture[] verts;
        public static VertexBuffer vertsBuffer;
        protected static IndexBuffer indexBuffer;
        protected static short[] vertsIndices;

        public static Texture2D texture;
        public static void initialise(GraphicsDevice graphics,ContentManager content)
        {
            texture = content.Load<Texture2D>(@"Assets\Textures\Abstract\objective");
            verts = new VertexPositionColorTexture[3];
         //   vertsIndices = new short[8];

            vertsBuffer = new VertexBuffer(graphics, typeof(VertexPositionColorTexture), verts.Length, BufferUsage.WriteOnly);
        //    indexBuffer = new IndexBuffer(graphics, typeof(short), vertsIndices.Length, BufferUsage.WriteOnly);
            verts[0] = new VertexPositionColorTexture(new Vector3(-1, 0, 0), Color.White, new Vector2(1, 0));
            verts[1] = new VertexPositionColorTexture(new Vector3(1, 0, 0), Color.White, new Vector2(0, 0));
            verts[2] = new VertexPositionColorTexture(new Vector3(0, -1, 0), Color.White, new Vector2(0.5f, 1));


            vertsBuffer.SetData(verts, 0, verts.Length);
        }
    }
}
