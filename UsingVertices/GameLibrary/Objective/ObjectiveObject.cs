﻿using Microsoft.Xna.Framework;

using UsingVertices;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class ObjectiveObject
    {
        //modifying this matrix modifies how the object is drawn on-screen
        //i.e. we can scale, rotate, and translate the object
        protected Matrix world;
        public static Main game;
        protected Matrix billboard;
        protected Matrix rotation;
        public bool rayPicked = false;

        protected Vector3 scale;
        protected Vector3 min;
        protected Vector3 max;

        protected Vector3 Position;

        public Texture2D directionTexture;

        protected bool updateThis = true;
        protected Texture2D texture;
        protected string name;
        public BoundingBox boundingBox;
        public BoundingSphere boundingSphere;
        protected WireframeBox debugBox;
        public bool bCollideable = false;
        protected bool bCollision = false;
        protected Vector3 centre;
        protected float radius;
        protected float distance = 0;
        protected BillBoard label;
        public bool active = false;

        public bool drawIndicator = false;
        protected Vector2 indicatorPos = Vector2.Zero;

        //protected Vector3 minPoint = Vector3.Zero, maxPoint = Vector3.Zero;
        #region PROPERTIES
        public Matrix ROTATION
        {
            get { return rotation; }
        }
        public Vector3 POSITION
        {
            get { return Position; }
            set { Position = value; }
        }
        public Matrix BillBoard
        {
            get { return billboard; }
            set { billboard = value; }
        }
        public Vector3 SCALE
        {
            get { return scale; }
        }
        public Matrix World
        {
            get
            {
                return world;
            }
            set
            {
                world = value;
            }
        }
        //returns a user-defined id for each object
        public string Name
        {
            get
            {
                return name;
            }
        }

        //returns the translational component of the world matrix i.e. the transform that affects position
        public Vector3 Translation
        {
            get
            {
                return world.Translation;
            }
            set
            {
                world.Translation = value;
            }
        }
        #endregion
        protected bool debugSphereAdded = false;
        public ObjectiveObject(string name, Vector3 Position, Vector3 scale, Vector3 minBB, Vector3 maxBB,Texture2D texture)
        {
            this.directionTexture = MiscAssets.indicator;
            this.texture = texture;
            this.scale = scale;
            this.min = minBB * scale;
            this.max = maxBB * scale;
            min.Z = 0;
            max.Z = -1;
            this.Position = Position;
            this.name = name; //id for each object
            this.centre = ((min + max) / 2) * scale;
            this.radius = max.X - centre.X;
            if (radius > 0)
            {
                this.boundingSphere = new BoundingSphere(centre + Position, radius);
            }
            this.rotation = Matrix.Identity;
            billboard = Matrix.Identity;
          
            //  addBoundingSphere();
        }
        protected void addBoundingSphere()
        {
            if (!debugSphereAdded)
                DebugShapeRenderer.AddBoundingSphere(this.boundingSphere, Color.White, 10000f);
            debugSphereAdded = true;
        }
        #region USED BY DRIVABLE OBJECTS
        //See MoveableCube and ThirdPersonCamera for use
        public virtual Vector3 getLook()
        {
            return Vector3.Zero; //dummy - will be overwritten in class that overrides this methods
        }
        public virtual Vector3 getUp()
        {
            return Vector3.Zero;  //dummy - will be overwritten in class that overrides this methods
        }
        public virtual Vector3 getRight()
        {
            return Vector3.Zero;  //dummy - will be overwritten in class that overrides this methods
        }
        #endregion


        public virtual void Initialize()
        {
        }
        public virtual void Update(GameTime gameTime)
        {
            Vector3 projct = Vector3.Zero;
            projct = game.GraphicsDevice.Viewport.Project(Vector3.Zero, game.cameraManager.ActiveCamera.Projection,
                game.cameraManager.ActiveCamera.View, world);
            indicatorPos = new Vector2(projct.X, projct.Y);

          //  System.Diagnostics.Debug.WriteLine("Project" + projct);
        
            drawIndicator = true;

            
            if (indicatorPos.X > game.GraphicsDevice.Viewport.Width)
            {
                indicatorPos.X = game.GraphicsDevice.Viewport.Width-40;
            }
            else if (indicatorPos.X < 0)
            {
                indicatorPos.X =  40;
            }
            if (indicatorPos.Y > game.GraphicsDevice.Viewport.Height)
            {
                indicatorPos.Y = game.GraphicsDevice.Viewport.Height - 40;
            }
            else if (indicatorPos.Y < 0)
            {
                indicatorPos.Y =  40;
            }
            if (projct.Z > 1f)
            {
                indicatorPos.Y = game.GraphicsDevice.Viewport.Height - 40;
            }

          //  if (HelperClass.onScreen(new Vector2(projct.X, projct.Y)) && projct.Z < 1f)
           // {
               // drawIndicator = false;
            //}
              
           
            //    if(!debugSphereAdded)
            // addBoundingSphere();

        }
        public virtual void onCollide(Object3D obj)
        {


        }
        public virtual void Draw(GameTime gameTime)
        {
            if (active)
            {
                if (drawIndicator)
                {
                    game.spriteBatch.Begin();
                    game.spriteBatch.Draw(directionTexture, new Rectangle((int)indicatorPos.X, (int)indicatorPos.Y - 20, 15,14), Color.Yellow);
                    game.spriteBatch.End();
                }
            }
        }
    }
}