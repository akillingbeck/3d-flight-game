﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace GameLibrary
{
    
    public class ObjectiveHome : ObjectiveObject
    {
     
        public ObjectiveHome(Vector3 position,Vector3 scale,Texture2D texture):
            base("Return Home",position,scale,TexturedCubeParams.minCorner,TexturedCubeParams.maxCorner,texture)
        {
            min = TexturedCubeParams.minCorner;
            max = TexturedCubeParams.maxCorner;
            boundingBox = new BoundingBox(min, max);
        }
        public override void Update(GameTime gameTime)
        {
            world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Position);
            boundingBox = new BoundingBox((min * scale) + Translation, (max * scale) + Translation);

            if (game.plane.boundingSphere.Intersects(this.boundingBox) && !AirPlane.gotHome)
            {
                AirPlane.gotHome = true;
               // game.plane.POSITIONY = this.max.Y + 5;
              //  game.plane.ROTATION = Matrix.Identity;
               // game.objectiveManager.currentObjective.COMPLETED = true;
                game.plane.velocity *= -1;
                game.plane.objectivesCompleted++;
                game.objectiveManager.nextObjective();
            }
            else if (game.plane.boundingSphere.Intersects(this.boundingBox))
            {
                game.plane.velocity *= -1;
                game.plane.health = 100;
                game.plane.armour = 100;
                game.plane.ammo = 500;
            }

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.textureEffect.World = world;
            game.textureEffect.Texture = texture;

            game.textureEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(TexturedCubeParams.vertsBuffer);
            game.GraphicsDevice.Indices = TexturedCubeParams.indexBuffer;
            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 24, 0, 12);
            base.Draw(gameTime);
        }
    }
}
