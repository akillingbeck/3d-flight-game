﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class ObjectiveArrow : Object3D
    {
        Texture2D texture;
        protected Vector3 direction;
        Vector3 oldDir;
        Vector3 Right, Up, Backwards,Look;
        protected Vector3 scale;
        protected Matrix rotation;
        public ObjectiveArrow(Vector3 scale):base("Objective Radar",Matrix.Identity)
        {
            texture = game.Content.Load<Texture2D>(@"Assets\Textures\Fence\fence1");
            direction = Vector3.Zero;
            Up = Vector3.Up;
            Right = Vector3.Right;
            Look = Vector3.Forward;
            oldDir = direction;
            this.scale = scale;
            rotation = Matrix.Identity;
        }
        public override void Draw(GameTime gameTime)
        {
            
            game.textureEffect.View = Matrix.Identity ;
            game.textureEffect.World = world;
            game.textureEffect.Texture = this.texture;
            game.textureEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(ObjectiveArrowParams.vertsBuffer);
           // game.GraphicsDevice.Indices = ObjectiveArrowParams.indexBuffer;
            game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>
                (PrimitiveType.TriangleStrip, ObjectiveArrowParams.verts,0,2);

            base.Draw(gameTime);

            game.textureEffect.View = game.cameraManager.ActiveCamera.View;
        }
        public override void Update(GameTime gameTime)
        {
            this.Translation = new Vector3(0, 1.5f, -5);
            //direction = game.objectiveManager.currentObjective.LOCATION - game.plane.Direction;
            //direction.Normalize();
            //direction.Y = 0;
       
            //    float dot = (float)Math.Acos(Vector3.Dot(Right, direction));
            //    Matrix rot = Matrix.Identity;
            //    if (dot != -1 )
            //    {
            //         rot = Matrix.CreateRotationY(dot);
            //        rotation *= rot;

                  
            //    }
            //    Look = Vector3.Transform(Look, rot);
            //    Look.Normalize();
            //    Right = Vector3.Cross(Up, Look);
            //    Right.Normalize();
            //    System.Diagnostics.Debug.WriteLine("Angle" + Right);
            Vector3 target = game.objectiveManager.currentObjective.LOCATION;
          //  Vector3 obj = game.plane.Direction;
            Vector3 vecToTarget = Vector3.Normalize(target -
                                       game.plane.Position);
                            double angle = Math.Acos((double)Vector3.Dot(game.plane.Direction,
                                                                         vecToTarget));
                            Vector3 axisToRotate = Vector3.Cross(game.plane.Direction,
                                                                 vecToTarget);
                            axisToRotate.Normalize();
                            Matrix rotationMatrix = Matrix.CreateFromAxisAngle(axisToRotate,
                                                   (float)angle);

          //  Matrix rot = ArrowMatrix(Translation,game.objectiveManager.currentObjective.LOCATION);
                            rotation *= rotationMatrix * game.delta;
            this.world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Translation);
             
           // base.Update(gameTime);
        }
        Matrix RotateToFace(Vector3 O, Vector3 P, Vector3 U)
        {
            Vector3 D = (O - P);
            Vector3 Right = Vector3.Cross(U, D);
            Vector3.Normalize(ref Right, out Right);
            Vector3 Backwards = Vector3.Cross(Right, U);
            Vector3.Normalize(ref Backwards, out Backwards);
            Vector3 Up = Vector3.Cross(Backwards, Right);
            Matrix rot = new Matrix(Right.X, Right.Y, Right.Z, 0, Up.X, Up.Y, Up.Z, 0, Backwards.X, Backwards.Y, Backwards.Z, 0, 0, 0, 0, 1);
            return rot;
        }
        public static Matrix ArrowMatrix(Vector3 ArrowPosition, Vector3 TargetPosition)
        {
            Vector3 tminusp = TargetPosition - ArrowPosition;
            Vector3 ominusp = Vector3.Backward;
            tminusp.Normalize();
            float theta = (float)System.Math.Acos(Vector3.Dot(tminusp, ominusp));
            Vector3 cross = Vector3.Cross(ominusp, tminusp);

            cross.Normalize();

            Quaternion targetQ = Quaternion.CreateFromAxisAngle(cross, theta);
            Matrix WorldMatrix = Matrix.CreateFromQuaternion(targetQ) * Matrix.CreateTranslation(ArrowPosition);

            return WorldMatrix;
        }
     
    }
}
