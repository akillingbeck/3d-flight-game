﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class ObjectivePickup : ObjectiveObject
    {
        Matrix spin;
        protected float timeAlive = 0;
        protected bool noTime = false;
        protected float nextObj = 0f;


        public ObjectivePickup(Vector3 position,Vector3 scale,Texture2D texture):
            base("ObjectivePickup", position,scale,TexturedCubeParams.minCorner,TexturedCubeParams.maxCorner,texture)
        {
            bCollideable = true;
            spin = Matrix.Identity;
            updateThis = false;
            min = TexturedCubeParams.minCorner;
            max = TexturedCubeParams.maxCorner;
            boundingBox = new BoundingBox(min, max);
           // label = new BillBoard(this, ObjectiveArrowParams.texture, new Vector3(0, 15f, 0), scale);
        }

        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                distance = (int)Vector3.Distance(POSITION, game.plane.POSITION);

                if (game.objectiveManager.currentObjective.TIMED)
                {

                    timeAlive += gameTime.ElapsedGameTime.Milliseconds;
                    if (timeAlive / 1000f > game.objectiveManager.currentObjective.TIME_TO_COMPLETE)
                    {
                        noTime = true;

                        game.objectiveManager.currentObjective.isActive = false;
                        timeAlive = 0;
                    }
                }
                spin = Matrix.CreateRotationY(MathHelper.ToRadians(2f));
                rotation *= spin;

                world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Position);
                boundingBox = new BoundingBox((min * scale) + Translation, (max * scale) + Translation);

                if (this.boundingBox.Intersects(game.plane.boundingSphere) && !noTime)
                {
                    MiscAssets.pickup.Play();
                    onCollide(game.plane);
                }
            }

                base.Update(gameTime);
            
        }
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                game.textureEffect.World = world;
                game.textureEffect.Texture = texture;

                game.textureEffect.CurrentTechnique.Passes[0].Apply();
                game.GraphicsDevice.SetVertexBuffer(TexturedCubeParams.vertsBuffer);
                game.GraphicsDevice.Indices = TexturedCubeParams.indexBuffer;
                game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 24, 0, 12);


                int timer = (int)(game.objectiveManager.currentObjective.TIME_TO_COMPLETE - (timeAlive / 1000f));
                game.spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, DepthStencilState.Default,
                    RasterizerState.CullNone);
                int minutes= timer / 60;
                 int seconds = 0;

                 if (minutes > 0)
                     seconds = timer - 60;
                 else
                 {
                     seconds = timer;
                 }

                if (!noTime)
                {
                    String text = "TIME LEFT: " + minutes + ":" + seconds;
                    String text2 = "" + distance + " M";
                    Vector2 textWidth = game.font.MeasureString(text);
                    Vector2 text2P = game.font.MeasureString(text2);
                    game.spriteBatch.DrawString(game.font, text, new Vector2(game.viewportCentre.X, textWidth.Y * 2f), Color.Blue,
                        0f, new Vector2(textWidth.X / 2, textWidth.Y / 2), 1f, SpriteEffects.None, 0f);

                    Vector3 proj = game.GraphicsDevice.Viewport.Project(Vector3.Zero, game.cameraManager.ActiveCamera.Projection, game.cameraManager.ActiveCamera.View, world);

                    if(proj.Z <1f)
                    game.spriteBatch.DrawString(game.font, text2, new Vector2(proj.X, proj.Y + 10f), Color.Green,
                          0f, new Vector2(text2P.X / 2, text2P.Y / 2), 1f, SpriteEffects.None, 0f);


                }
                else
                {
                    String text = "You ran out of time!";
                    Vector2 textWidth = game.font.MeasureString(text);
                    game.spriteBatch.DrawString(game.font, text, new Vector2(game.viewportCentre.X, textWidth.Y * 2f), Color.Red,
                           0f, new Vector2(textWidth.X / 2, textWidth.Y / 2), 3f, SpriteEffects.None, 0f);
                    nextObj += gameTime.ElapsedGameTime.Milliseconds;

                    if (nextObj >= 3000f)
                    {


                        if (game.objectiveManager.currentObjective.objectives != null)
                        {
                            game.objectiveManager.currentObjective.objectives.Remove(this);
                            if (game.objectiveManager.currentObjective.objectives.Count == 0)
                            {
                                game.plane.objectivesFailed++;
                                game.objectiveManager.nextObjective();
                            }
                        }
                        else
                        {
                            game.objectiveManager.currentObjective.objective = null;
                            game.plane.objectivesFailed++;
                            game.objectiveManager.nextObjective();
                        }
                        game.billBoardManager.Remove(this.label);
                        nextObj = 0;

                    }
                    bCollideable = false;
                }
                game.spriteBatch.End();
            }
                base.Draw(gameTime);
            
        }
        public override void onCollide(Object3D obj)
        {
            if (obj is AirPlane)
            {
               

                if (game.objectiveManager.currentObjective.objectives != null)
                {
                    game.objectiveManager.currentObjective.objectives.Remove(this);
                    if (game.objectiveManager.currentObjective.objectives.Count == 0)
                    {
                        game.plane.objectivesCompleted++;
                        game.objectiveManager.nextObjective();
                    }
                }
                else
                {
                    game.objectiveManager.currentObjective.objective = null;
                    game.plane.objectivesCompleted++;
                    game.objectiveManager.nextObjective();
                }
                game.billBoardManager.Remove(this.label);
               
               
       
            }
            base.onCollide(obj);
        }
    }
}
