﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using UsingVertices;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class Objective
    {

        public List<ObjectiveObject> objectives;
        public int listSize;
        protected bool multipleObjectives = false;


        public static Main game;
        protected bool completed = false;
        protected bool timed = false;
        protected bool doBob = false;
        public bool isActive = true;
        protected Vector3 location;
        protected int timeToComplete = 0;
        protected String name;
       public ObjectiveObject objective;
       public Vector3 direction;
      
       public Matrix world;

       public int currentIndex = 0;
       Matrix rotation;
       protected Vector3 scale;
        #region PROPERTIES
        public String OBJECTIVE_NAME
        {
            get { return this.name; }
        }
        public Vector3 LOCATION
        {
            get { return location; }
            set { location = value; }
        }
        public Boolean COMPLETED
        {
            get { return completed; }
            set { completed = value; }
        }
        public int TIME_TO_COMPLETE
        {
            get { return timeToComplete; }
        }
        public bool TIMED
        {
            get { return timed; }
            set { timed = value; }
        }
        #endregion
        public Objective(String name,Vector3 location,int timeToComplete, ObjectiveObject objective)
        {
            this.name = name;

         
            if (objective == null)
            {
                this.location = location;
               
            }
            else
            {
                multipleObjectives = false;
                this.objective = objective;
                this.location = objective.Translation + new Vector3(0,10f,0);
                direction = Vector3.Normalize(objective.Translation - this.location);
            }
            
            this.timeToComplete = timeToComplete;

            if (this.timeToComplete > 0)
            {
                timed = true;
            }
            COMPLETED = false;
            rotation = Matrix.CreateRotationX(MathHelper.ToRadians(-90));
           
           
            game.objectiveManager.add( this);
            
        }
        public Objective(String name, Vector3 location, int timeToComplete, List<ObjectiveObject> list)
        {
            this.name = name;

            if (list != null)
            {
                this.objectives = list;
                multipleObjectives = true;
            }

            this.timeToComplete = timeToComplete;

            if (this.timeToComplete > 0)
            {
                timed = true;
            }
            COMPLETED = false;
            rotation = Matrix.CreateRotationX(MathHelper.ToRadians(-90));

            listSize = list.Count;
            game.objectiveManager.add(this);

        }
        
        public virtual void Update(GameTime gameTime)
        {


            if (objective != null)
            {
                if (!multipleObjectives)
                {
                    objective.active = true;
                    objective.Update(gameTime);
                }
            }

            if (objectives != null)
            {
                if (multipleObjectives)
                {

                    for (int i = 0; i < objectives.Count; i++)
                    {
                        objectives[i].active = true;
                        objectives[i].Update(gameTime);


                    }
        
                }
            }
       
        }

        public virtual void Draw(GameTime gameTime)
        {

            if (isActive)
            {
                //game.textureEffect.World = world;
                //game.textureEffect.Texture = ObjectiveArrowParams.texture;
                //game.textureEffect.CurrentTechnique.Passes[0].Apply();
                //game.GraphicsDevice.SetVertexBuffer(ObjectiveArrowParams.vertsBuffer);
                //// game.GraphicsDevice.Indices = ObjectiveArrowParams.indexBuffer;
                //game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>
                //    (PrimitiveType.TriangleStrip, ObjectiveArrowParams.verts, 0, 1);
            }

            if (objective != null)
            {
                if (!multipleObjectives)
                    objective.Draw(gameTime);
            }
            if(objectives != null)
            {
                for (int i = 0; i < objectives.Count; i++)
                {
                    objectives[i].Draw(gameTime);
                }
            }
        }

    }
}
