﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using UsingVertices;

namespace GameLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class MouseManager : Microsoft.Xna.Framework.GameComponent
    {
        MouseState oldState, newState;

        public Vector2 Position
        {
            get
            {
                return new Vector2(newState.X, newState.Y);
            }
        }
        public MouseState NEWSTATE
        {
            get
            {
                return newState;
            }
        }

        public MouseState OLDSTATE
        {
            get
            {
                return oldState;
            }
        }

        public MouseManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            newState = Mouse.GetState();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        public int getScrollWheelValue()
        {
            return newState.ScrollWheelValue;
        }

        public bool isLeftButtonPressed()
        {
            return newState.LeftButton.Equals(ButtonState.Pressed);
        }

        public bool isMiddleButtonPressed()
        {
            return newState.MiddleButton.Equals(ButtonState.Pressed);
        }

        public bool isRightButtonPressed()
        {
            return newState.RightButton.Equals(ButtonState.Pressed);
        }

        //are the buttons pressed now but released in the previous update
        public bool isLeftButtonPressedOnce()
        {
            return(newState.LeftButton.Equals(ButtonState.Pressed) && (oldState.LeftButton.Equals(ButtonState.Released)));
        }

        public bool isMiddleButtonPressedOnce()
        {
            return (newState.MiddleButton.Equals(ButtonState.Pressed) && (oldState.MiddleButton.Equals(ButtonState.Released)));
        }

        public bool isRightButtonPressedOnce()
        {
            return (newState.RightButton.Equals(ButtonState.Pressed) && (oldState.RightButton.Equals(ButtonState.Released)));
        }

        public bool isMouseStateChanged()
        {
            return (!newState.Equals(oldState)); //are states different?
        }

        public int getScrollWheelDelta()
        {
            if (!newState.Equals(oldState))
            {
                return newState.ScrollWheelValue - oldState.ScrollWheelValue;
            }
            return 0;
        }

        public override void Update(GameTime gameTime)
        {
            oldState = newState;

            newState = Mouse.GetState();

            base.Update(gameTime);
        }
    }
}