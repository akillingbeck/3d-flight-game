﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class BillBoardManager : DrawableGameComponent
    {
        // list containing all cameras
        List<BillBoard> billboardList;

        public int Length
        {
            get
            {
                return billboardList.Count;
            }
        }
        public List<BillBoard> Objects
        {
            get
            {
                return billboardList;
            }
        }

        public BillBoard this[int index]
        {
            get
            {
                return billboardList.ElementAt<BillBoard>(index);
            }
        }

        public int Count
        {
            get
            {
                return billboardList.Count;
            }
        }

        public void Clear()
        {
            billboardList.Clear();
        }

        public void Add(BillBoard billboard)
        {
            billboardList.Add(billboard);
        }

        public void Remove(BillBoard billboard)
        {
            billboardList.Remove(billboard);
        }

        public void Remove(int index)
        {
            billboardList.RemoveAt(index);
        }


        public BillBoardManager(Main game)
            : base(game)
        {
            this.billboardList = new List<BillBoard>(1);

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // Loop through all models and call Update
            for (int i = 0; i < billboardList.Count; ++i)
            {
                billboardList.ElementAt<BillBoard>(i).Update(gameTime);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //if we dont enable depth buffer then components will be drawn in order they were
            //added as components and not on their order in 3D space
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            //setting this to none means that triangles with  back facing normals will not be drawn
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            //enable alpha blending for the on-screen content (i.e. if it has alpha value on texture then it will appear 
            //as something between transparent (alpha = 0) or opaque (alpha = 1))
            GraphicsDevice.BlendState = BlendState.AlphaBlend;

            // Loop through and draw each model
            for (int i = 0; i < billboardList.Count; ++i)
            {
                billboardList.ElementAt<BillBoard>(i).Draw(gameTime);
            }

            base.Draw(gameTime);
        }
    }
}