using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class CameraManager : Microsoft.Xna.Framework.GameComponent
    {
        // Store active light
        int activeCameraIndex;
        Camera activeCamera;

        Game game;
        //list containing all cameras
        List<Camera> cameras;

        #region Properties

        public string ActiveCameraType
        {
            get
            {
                return cameras[activeCameraIndex].GetType().Name;
            }
        }

        // Index of the active camera
        public int ActiveCameraIndex
        {
            get
            {
                return activeCameraIndex;
            }
        }

        public Camera ActiveCamera
        {
            get
            {
                return activeCamera;
            }
        }

      /*  public Camera this[int index]
        {
            get
            {
                return cameras.Values[index];
            }
        }

        public Camera this[string id]
        {
            get
            {
                return cameras[id];
            }
        }
        */

        public int Count
        {
            get
            {
                return cameras.Count;
            }
        }
        #endregion

        public CameraManager(Game game, int cameraCount)
            : base(game)
        {
            this.game = game;
            cameras = new List<Camera>(cameraCount);
            activeCameraIndex = -1;
        }

        public void SetActiveCamera(int cameraIndex)
        {
            activeCameraIndex = cameraIndex;
            activeCamera = cameras[cameraIndex];
        }

     

        public void Cycle()
        {
            if (activeCameraIndex < Count-1)
            {
                //pre-increment and use
                SetActiveCamera(++activeCameraIndex);
            }
            else if (activeCameraIndex == Count-1)
            {
                SetActiveCamera(0);
            }
        }
        public void Clear()
        {
            cameras.Clear();
            activeCamera = null;
            activeCameraIndex = -1;
        }

        public void Add(Camera camera)
        {
            cameras.Add(camera);

            if (activeCamera == null)
            {
                activeCamera = camera;
                activeCameraIndex = 0;
            }
        }

        public void Remove(int index)
        {
            cameras.RemoveAt(index);
        }

        public override void Update(GameTime gameTime)
        {
            ActiveCamera.Update(gameTime);
            base.Update(gameTime);
        }
    }
}
