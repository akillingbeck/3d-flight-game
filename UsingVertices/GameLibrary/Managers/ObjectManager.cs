using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class ObjectManager : DrawableGameComponent
    {
        // list containing all cameras
        List<Object3D> objectList;

        public int Length
        {
            get
            {
                return objectList.Count;
            }
        }
        public List<Object3D> Objects
        {
            get
            {
                return objectList;
            }
        }

        public Object3D this[int index]
        {
            get
            {
                return objectList.ElementAt<Object3D>(index);
            }
        }

        public int Count
        {
            get
            {
                return objectList.Count;
            }
        }

        public void Clear()
        {
            objectList.Clear();
        }

        public void Add(Object3D object3D)
        {
            objectList.Add(object3D);
        }

        public void Remove(Object3D object3D)
        {
            objectList.Remove(object3D);
        }

        public void Remove(int index)
        {
            objectList.RemoveAt(index);
        }


        public ObjectManager(Main game)
            : base(game)
        {
            this.objectList = new List<Object3D>(1);

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // Loop through all models and call Update
            for (int i = 0; i < objectList.Count; ++i)
            {
                objectList.ElementAt<Object3D>(i).Update(gameTime);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //if we dont enable depth buffer then components will be drawn in order they were
            //added as components and not on their order in 3D space
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            //setting this to none means that triangles with  back facing normals will not be drawn
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            //enable alpha blending for the on-screen content (i.e. if it has alpha value on texture then it will appear 
            //as something between transparent (alpha = 0) or opaque (alpha = 1))
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            
            // Loop through and draw each model
            for (int i = 0; i < objectList.Count; ++i)
            {
                objectList.ElementAt<Object3D>(i).Draw(gameTime);
            }

            base.Draw(gameTime);
        }
    }
}