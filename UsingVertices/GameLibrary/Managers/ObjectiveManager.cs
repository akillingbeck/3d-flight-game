﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using UsingVertices;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class ObjectiveManager : DrawableGameComponent
    {
        private List<Objective> objectives;
        public Objective currentObjective;
        public int currentObjectiveInt;
        public static bool allComplete = false;

        public int Count
        {
            get { return objectives.Count; }
        }
        public ObjectiveManager(Main game):base(game)
        {
            this.objectives = new List<Objective>();
            currentObjectiveInt = -1;
        }

        public void setActiveObjective(int objectiveIndex)
        {
            currentObjectiveInt = objectiveIndex;
            currentObjective = objectives[objectiveIndex];

        }
        public void nextObjective()
        {
            
            if (currentObjectiveInt < objectives.Count -1 )
            {
                //pre-increment and use
                setActiveObjective(++currentObjectiveInt);
            }
            else if (currentObjectiveInt == objectives.Count - 1)
            {
                setActiveObjective(0);
                allComplete = true;
               
            }
        }
        public void add(Objective obj)
        {
            objectives.Add(obj);

            if (currentObjective == null)
            {
                currentObjective = obj;
                currentObjectiveInt = 0;
            }
        }
        public void remove(Objective obj)
        {
           
            objectives.Remove(obj);
          //  nextObjective();
         
        }
        public void remove(int index)
        {

            objectives.RemoveAt(index);
        }
        public void clear()
        {
            objectives.Clear();
        }

        public override void Update(GameTime gameTime)
        {
            //if (currentObjectiveInt == objectives.Count)
            //{
            //    allComplete = true;
            //}

            if (allComplete)
            {
                objectives.Clear();
            }
            // Loop through all models and call Update
         //   for (int i = 0; i < objectives.Count; ++i)
         //   {

            if (null != currentObjective && !currentObjective.COMPLETED)
            {
                currentObjective.Update(gameTime);
                
               // currentObjective.objective.Update(gameTime);
            }
            
          //  }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //if we dont enable depth buffer then components will be drawn in order they were
            //added as components and not on their order in 3D space
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            //setting this to none means that triangles with  back facing normals will not be drawn
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            //enable alpha blending for the on-screen content (i.e. if it has alpha value on texture then it will appear 
            //as something between transparent (alpha = 0) or opaque (alpha = 1))
            GraphicsDevice.BlendState = BlendState.AlphaBlend;

            // Loop through and draw each model
            if (null != currentObjective && !currentObjective.COMPLETED)
            {
                currentObjective.Draw(gameTime);
              //  currentObjective.objective.Draw(gameTime);
            }
            

            base.Draw(gameTime);
        }

    }
}
