﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;


namespace GameLibrary
{
    /*
     * Function:    Stores all the tracks that trackcameras will use
     * Author:      NMCG
     * Date:        19/2/11
     * Version:     1.0
     * Revisions:   None
     */

    public class TrackManager 
    {
        // list containing all cameras
        protected List<Track> trackList;
        protected Track activeTrack;
        protected int activeTrackIndex = 0;

        public Track ActiveTrack
        {
            get
            {
                return activeTrack;
            }
        }


        public TrackManager()

        {
            this.trackList = new List<Track>(1);
        }
        public List<Track> Objects
        {
            get
            {
                return trackList;
            }
        }

        public Track this[int index]
        {
            get
            {
                return trackList.ElementAt<Track>(index);
            }
        }

        public int Count
        {
            get
            {
                return trackList.Count;
            }
        }

        public void Clear()
        {
            trackList.Clear();
        }

        public void Add(Track aTrack)
        {
            trackList.Add(aTrack);

            if (activeTrack == null)
            {
                activeTrack = aTrack;
                activeTrackIndex = 0;
            }
        }

        public void SetActiveTrack(Vector3 position)
        {
            //UNDER CONSTRUCTION...
        }

        public void SetActiveTrack(int trackIndex)
        {
            activeTrackIndex = trackIndex;
            activeTrack = trackList[trackIndex];
        }

        public void Remove(Track aTrack)
        {
            trackList.Remove(aTrack);
        }

        public void Remove(int index)
        {
            trackList.RemoveAt(index);
        }

    }
}