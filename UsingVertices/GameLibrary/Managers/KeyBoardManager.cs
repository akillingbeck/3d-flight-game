using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


namespace GameLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class KeyBoardManager : Microsoft.Xna.Framework.GameComponent
    {
        KeyboardState oldState, newState;

        public KeyboardState State
        {
            get
            {
                return newState;
            }
        }

        public KeyBoardManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            newState = Keyboard.GetState();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        public bool isStateChanged()
        {
            return !newState.Equals(oldState);
        }

        public bool isKeyDown(Keys key)
        {
            return newState.IsKeyDown(key);
        }

        public bool isKeyPressed(Keys key)
        {
            if (newState.IsKeyDown(key) && (oldState.IsKeyUp(key)))
            {
                return true;
            }

            return false;
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            oldState = newState;

            newState = Keyboard.GetState();

            base.Update(gameTime);
        }
    }
}