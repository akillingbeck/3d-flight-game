﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
    public class GamePerformance : DrawableGameComponent
    {
        SpriteFont spriteFont;

        int frameRate = 0;
        int frameCount = 0;
        TimeSpan elapsedTime;

        public GamePerformance(Main game, SpriteFont spriteFont)
            : base(game)
        {
            this.spriteFont = spriteFont;
            this.elapsedTime = TimeSpan.Zero;
        }


        public override void Update(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCount;
                frameCount = 0;
            }
          //  System.Diagnostics.Debug.WriteLine("FPS "+frameRate);
        }


        public override void Draw(GameTime gameTime)
        {
            frameCount++;
            ((Main)Game).spriteBatch.Begin();
            ((Main)Game).spriteBatch.DrawString(spriteFont, "FPS: " + frameRate, GameBehaviour.DEBUG_MENU_FPS_POSITION, GameBehaviour.DEBUG_MENU_FPS_COLOR);
            ((Main)Game).spriteBatch.End();
        }
    }
}
