﻿using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public abstract class GameBehaviour
    {
        #region DEBUG;
        public static bool DEBUG_MENU_SHOW = true;
        public static string DEBUG_MENU_FONT = "GamePerformance";
        public static Vector2 DEBUG_MENU_FPS_POSITION = new Vector2(600, 400);
        public static Color DEBUG_MENU_FPS_COLOR = Color.White;
        #endregion;

    }
}
