﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameLibrary
{
    public class Integer2
    {
        protected int x, y;

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public static Integer2 Zero = new Integer2(0, 0);
        public static Integer2 One = new Integer2(1, 1);
        public static Integer2 UnitX = new Integer2(1, 0);
        public static Integer2 UnitY = new Integer2(0, 1);

        public Integer2(int x, int y)
        {
            this.x = x; this.y = y;
        }

        public String toString()
        {
            return x + "," + y;
        }
    }
}
