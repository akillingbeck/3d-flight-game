﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using UsingVertices;

namespace GameLibrary
{
    public class HelperClass
    {
        public static Main game;
        public static bool onScreen(Vector2 p1)
        {
            if ((p1.X > 0 && p1.X < game.GraphicsDevice.Viewport.Width) && p1.Y >0 && p1.Y < game.GraphicsDevice.Viewport.Height)
            {
                return true;
            }
            return false;
        }
    }
}
