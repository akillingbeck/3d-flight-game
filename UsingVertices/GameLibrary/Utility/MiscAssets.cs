﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace GameLibrary
{
   public class MiscAssets
    {
       public static Texture2D hudBg;
       public static Texture2D indicator;
       public static Texture2D army;
       public static Texture2D buildingType1;
       public static Texture2D buildingType2;
       public static Texture2D buildingType3;
       public static Texture2D buildingType4;
       public static Texture2D buildingType5;
       public static Texture2D heliPad;
       public static Texture2D cargoTexture;
       public static Texture2D enemyShip;
       public static Texture2D repairShop;

       public static SoundEffect shoot;
       public static SoundEffect bump;
       public static SoundEffect hit;
       public static SoundEffect pickup;
       public static SoundEffect repair;
       public static SoundEffect hum;
       

       public static void load(ContentManager Content)
       {
           hudBg = Content.Load<Texture2D>(@"Assets\Textures\Scenery\hudBG");
           indicator = Content.Load<Texture2D>(@"Assets\Textures\hudIndicator");
           army = Content.Load<Texture2D>(@"Assets\Textures\military_camo");
           buildingType1 = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Structures\buildingType1");
           buildingType2 = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Structures\buildingType2");
           buildingType3 = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Structures\buildingType3");
           buildingType4 = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Structures\buildingType4");
           buildingType5 = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Structures\buildingType5");
           heliPad = Content.Load<Texture2D>(@"Assets\Textures\Scenery\Structures\helipad");
           cargoTexture = Content.Load<Texture2D>(@"Assets\Textures\Props\cargo");
           enemyShip = Content.Load<Texture2D>(@"Assets\Textures\enemyShip");
           repairShop = Content.Load<Texture2D>(@"Assets\Textures\Abstract\repairShop");
           shoot = Content.Load<SoundEffect>(@"Assets\laser");
           repair = Content.Load<SoundEffect>(@"Assets\repairSound");
           hit = Content.Load<SoundEffect>(@"Assets\hit");
           pickup = Content.Load<SoundEffect>(@"Assets\pickup");
         
       }
    }
}
