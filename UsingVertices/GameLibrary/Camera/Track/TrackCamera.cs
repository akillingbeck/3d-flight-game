using Microsoft.Xna.Framework;


namespace GameLibrary
{
    /*
     * Function:    Camera that moves along a user-defined track
     * Author:      NMCG
     * Date:        11/2/12
     * Version:     1.0
     * Revisions:   None
     */
    public class TrackCamera : Camera
    {
        protected Object3D targetObject;
        private Vector3 newPosition; //store new position of the camera after moving

        public TrackCamera(Object3D targetObject)
            : base(Vector3.Zero, targetObject.Translation, Vector3.UnitY) //set initial dummy Up value this will be changed
        {
            this.targetObject = targetObject;
            this.Position = game.trackManager.ActiveTrack.START;
            this.Look = targetObject.Translation - Position;
            this.Up = Vector3.Cross(game.trackManager.ActiveTrack.RIGHT, Look);
        }

        public override void Update(GameTime gameTime)
        {



            float dot = Vector3.Dot((targetObject.Translation - Position), game.trackManager.ActiveTrack.RIGHT);
            newPosition = Position + dot * game.trackManager.ActiveTrack.RIGHT * game.delta;

            //only allow the camera to move along the rail
            if ((Vector3.Distance(newPosition, game.trackManager.ActiveTrack.START) <= game.trackManager.ActiveTrack.LENGTH)
                && (Vector3.Distance(newPosition, game.trackManager.ActiveTrack.END) <= game.trackManager.ActiveTrack.LENGTH))
            {
                this.Position = newPosition;
            }
            //update look and up vector even if the target is outside the rail start-end
            this.Look = Vector3.Normalize(targetObject.Translation - this.Position);
            this.Up = Vector3.Cross(game.trackManager.ActiveTrack.RIGHT, Look);
            updateView();
       
        }
    }
}

