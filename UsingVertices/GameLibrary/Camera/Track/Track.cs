﻿using Microsoft.Xna.Framework;

namespace GameLibrary
{

    /*
     * Function:    Stores all the attributes of a track
     * Author:      NMCG
     * Date:        11/2/12
     * Version:     1.0
     * Revisions:   None
     */

    public class Track
    {
        protected static int trackCount = 0; //counts number of tracks
        protected int id = 0;  //ID for each track
        protected string trackName;

        protected Vector3 start, end, center, right;
        protected float length;

        public int ID
        {
            get
            {
                return id;
            }
        }
        public string NAME
        {
            get
            {
                return trackName;
            }
        }
        public Vector3 START
        {
            get
            {
                return start;
            }
        }
        public Vector3 END
        {
            get
            {
                return end;
            }
        }
        public Vector3 CENTER
        {
            get
            {
                return center;
            }
        }
        public Vector3 RIGHT
        {
            get
            {
                return right;
            }
        }
        public float LENGTH
        {
            get
            {
                return length;
            }
        }

        public Track(string trackName, Vector3 start, Vector3 end)
        {
            this.start = start;
            this.end = end;
            this.center = (start + end) / 2;
            this.length = Vector3.Distance(start, end);
            this.right = Vector3.Normalize(end - start);

            //every track as a unique and ID because every camera is associated with a single track
            this.trackName = trackName;
            this.id = trackCount;
            trackCount++;
        }
    }
}
