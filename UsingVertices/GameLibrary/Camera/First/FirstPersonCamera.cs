using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace GameLibrary
{
    public class FirstPersonCamera : Camera
    {
        //stores the amount of yaw(y-axis) and pitch(x-axis)
        private Matrix cameraRotation;

        private float leftrightRot = 0, updownRot = 0;

        //store the original orientation so that we perform all rotations with original values
        private Vector3 originalTarget, originalUp;

        //note: when using the firstpersoncamera we specify a target DIRECTION
        //NOT a target position (e.g. (1,0,0) and not (10,0,0))
        public FirstPersonCamera(Vector3 pos, Vector3 targetDirection, Vector3 up)
            : base(pos, targetDirection + pos, up)
        {
            //turn off the mouse
            game.IsMouseVisible = true;

            //store the original target and up
            this.originalTarget = targetDirection;
            this.originalUp = up;

            //centre mouse on the screen
            Mouse.SetPosition(game.viewportCentre.X, game.viewportCentre.Y);

        }


        public override void Update(GameTime gameTime)
        {        
            updateFromMouseInput();

            updateFromKeyboardInput();

            //centre mouse on the screen
            Mouse.SetPosition(game.viewportCentre.X, game.viewportCentre.Y);

            updateView();

        }

        private void updateFromMouseInput()
        {
            leftrightRot -= (game.mouseManager.NEWSTATE.X - game.viewportCentre.X) * CameraBehaviour.FIRSTPERSON_YAW_ANGLE * game.delta; // pitch
            updownRot -= (game.mouseManager.NEWSTATE.Y - game.viewportCentre.Y) * CameraBehaviour.FIRSTPERSON_YAW_ANGLE * game.delta; // yaw
               
            cameraRotation = Matrix.CreateFromYawPitchRoll(leftrightRot, updownRot, 0);
            Up = Vector3.Transform(originalUp, cameraRotation);
            Look = Vector3.Normalize(Vector3.Transform(originalTarget, cameraRotation));
        }

        private void updateFromKeyboardInput()
        {

            if (game.keyboardManager.isKeyDown(CameraBehaviour.FIRSTPERSON_FORWARD_KEY))
            {
                Position += CameraBehaviour.FIRSTPERSON_WALK_FORWARD_SPEED * Look * game.delta;
            }
            else if (game.keyboardManager.isKeyDown(CameraBehaviour.FIRSTPERSON_BACKWARD_KEY))
            {
                Position -= CameraBehaviour.FIRSTPERSON_WALK_BACKWARD_SPEED * Look * game.delta;
            }
            if (game.keyboardManager.isKeyDown(CameraBehaviour.FIRSTPERSON_LEFT_KEY))
            {
                Position += CameraBehaviour.FIRSTPERSON_STRAFE_SPEED * Right * game.delta;
            }
            else if (game.keyboardManager.isKeyDown(CameraBehaviour.FIRSTPERSON_RIGHT_KEY))
            {
                Position -= CameraBehaviour.FIRSTPERSON_STRAFE_SPEED * Right * game.delta;
            }
        }
    }
}