﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace GameLibrary
{
    public class FollowCamera : Camera
    {
        protected float angleElevation, distance;
        protected Object3D targetObject;
        public FollowCamera(Object3D targetObject, float angleElevation, float distance)
            : base(Vector3.One, targetObject.Translation, Vector3.UnitY)
        {
            //reset the position, target, and up
            this.targetObject = targetObject;
            this.angleElevation = angleElevation;
            this.distance = distance;
        }
        public override void Update(GameTime gameTime)
        {
            //get scroll wheel position
            int newScrollWheelValue = game.mouseManager.getScrollWheelValue();


            Vector3 rotatedLook = Vector3.Transform(-targetObject.getLook(),
                Matrix.CreateFromAxisAngle(targetObject.getRight(), angleElevation));
            rotatedLook.Normalize();
            //set camera position...
            this.Position = targetObject.Translation + rotatedLook * distance;
            this.Target = targetObject.Translation;

            this.Look = Vector3.Normalize(targetObject.Translation - this.Position);

            //Killingbeck Solution (c) 2012
          //  this.Up = Vector3.Normalize(Vector3.Cross(this.Look, this.Right));
            updateView();
        }
    }
}