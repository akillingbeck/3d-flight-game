﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class FlightCamera : Camera
    {

        public float Stiffness
        {
            get { return stiffness; }
            set { stiffness = value; }
        }
        private float stiffness = 1800.0f;

        /// <summary>
        /// Physics coefficient which approximates internal friction of the spring.
        /// Sufficient damping will prevent the spring from oscillating infinitely.
        /// </summary>
        public float Damping
        {
            get { return damping; }
            set { damping = value; }
        }
        private float damping = 600.0f;

        /// <summary>
        /// Mass of the camera body. Heaver objects require stiffer springs with less
        /// damping to move at the same rate as lighter objects.
        /// </summary>
        public float Mass
        {
            get { return mass; }
            set { mass = value; }
        }
        private float mass = 50.0f;



        protected AirPlane plane;


   
        private Vector3 velocity;

        private Vector3 desiredPos;
        private Vector3 cameraOffset = new Vector3(0, 2.0f, 10.0f);
        private Vector3 lookOffset = new Vector3(0, 2.4f, 0);


        public Vector3 Velocity
        {
            get { return velocity; }
        }
        public Vector3 LookOffset
        {
            get { return lookOffset; }
            set { lookOffset = value; }
        }

        public Vector3 CameraOffset
        {
            get
            {
                return cameraOffset;
            }
            set
            {
                cameraOffset = value;
            }
        }

        public Vector3 DesiredPosition
        {
            get
            {
                // Ensure correct value even if update has not been called this frame
                UpdatePositions();

                return desiredPos;
            }
        }


          public FlightCamera(Object3D targetObject)
            : base(targetObject.Translation, targetObject.Translation, Vector3.UnitY)
        {
            this.plane = (AirPlane)targetObject;
        
              game.IsMouseVisible = false;

        }


          public override void Reset()
          {
              UpdatePositions();

              velocity = Vector3.Zero;
              Position = desiredPos;
              this.updateView();
              base.Reset();
          }
          public override void Update(GameTime time)
          {
              UpdatePositions();

            

              Vector3 stretch = Position - desiredPos;
              Vector3 force = -stiffness * stretch - damping * velocity;

              // Apply acceleration
              Vector3 acceleration = force / mass;
              velocity += acceleration * game.delta;

              Position += velocity * game.delta;

              updateView();
          }

          public void UpdatePositions()
          {

              Matrix transform = Matrix.Identity;
              transform.Forward = plane.Direction;
              transform.Up = Up;
              transform.Right = Vector3.Cross(Up, plane.Direction);

              desiredPos = plane.POSITION + Vector3.Transform(cameraOffset, transform);

              Look = plane.POSITION + Vector3.Transform(lookOffset, transform);
          }
          protected override void  updateView()
          {
                this.viewMatrix = Matrix.CreateLookAt(this.Position, this.Look, this.Up);

                updateFrustum();
          }
         
    }
}
