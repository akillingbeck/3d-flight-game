using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
    public abstract class Camera
    {
        //Projection vector components
        protected float fieldOfView;
        protected float aspectRatio;
        protected float nearClipPlane;
        protected float farClipPlane;
   
        // View vector components
        protected Vector3 cameraPosition;
        protected Vector3 cameraTarget;
        protected Vector3 cameraLook;
        protected Vector3 cameraUp;
        protected Vector3 cameraRight;
        protected Matrix viewMatrix;
        protected Matrix projectionMatrix;

        //used to specify if the view and projection need updating
        protected bool bUpdateView;
        protected bool bUpdateProjection;

        //used to determine if an object is inside the camera frustum and should be drawn
        protected BoundingFrustum boundingFrustum;
        //used to detect if camera is colliding with an object
        //protected BoundingSphere boundingSphere;

        //used by all camera to access main and get access to the managers.
        public static Main game;



        #region CAMERA_PROPERTIES;

        public float FOV
        {
            get
            {
                return fieldOfView;
            }
            set
            {
                fieldOfView = value;
                bUpdateProjection = true;
            }
        }
        public float AspectRatio
        {
            get
            {
                return aspectRatio;
            }
            set
            {
                aspectRatio = value;
                bUpdateProjection = true;
            }
        }
        public float NearPlane
        {
            get
            {
                return nearClipPlane;
            }
            set
            {
                nearClipPlane = value;
                bUpdateProjection = true;
            }
        }
        public float FarPlane
        {
            get
            {
                return farClipPlane;
            }
            set
            {
                farClipPlane = value;
                bUpdateProjection = true;
            }
        }
        public Vector3 Position
        {
            get
            {
                return cameraPosition;
            }
            set
            {
                cameraPosition = value;
                bUpdateView = true;
            }
        }
        public Vector3 Target
        {
            get
            {
                return cameraTarget;
            }
            set
            {
                cameraTarget = value;
                bUpdateView = true;
            }
        }
        public Vector3 Look
        {
            get
            {
                return cameraLook;
            }
            set
            {
                cameraLook = value;
                bUpdateView = true;
                //update the right vector
                cameraRight = Vector3.Cross(Up, Look);
            }
        }
        public Vector3 Up
        {
            get
            {
                return cameraUp;
            }
            set
            {
                cameraUp = value;
                bUpdateView = true;
                //update the right vector
                cameraRight = Vector3.Cross(Up, Look);
            }
        }
        public Vector3 Right
        {
            get
            {
                return cameraRight;
            }
            set
            {
                cameraRight = value;
            }
        }



        public Matrix View 
        { 
            get
            {
                if(bUpdateView)
                {
                    updateView();        
                }
                return viewMatrix;
            }
        }
        public Matrix Projection
        {
            get
            {
                if (bUpdateProjection)
                {
                    updateProjection();
                }
                return projectionMatrix;
            }
        }

        public BoundingFrustum FRUSTUM
        {
            get
            {
                return boundingFrustum;
            }
        }
        public BoundingSphere BOUNDINGSPHERE
        {
            get
            {
                return new BoundingSphere(Position, CameraBehaviour.CAMERA_BOUNDINGSPHERE_RADIUS);
            }
        }

       #endregion;

        public Camera(Vector3 pos, Vector3 target, Vector3 up)
        {
            //set defaults for projection
            this.fieldOfView = CameraBehaviour.FOV_IN_DEGREES;
            this.aspectRatio = CameraBehaviour.ASPECT_RATIO;
            this.nearClipPlane = CameraBehaviour.NEAR_CLIP_PLANE;
            this.farClipPlane = CameraBehaviour.FAR_CLIP_PLANE;

            // Build camera view matrix
            this.cameraPosition = pos;
            this.cameraTarget = target;
            this.cameraUp = up;
            this.cameraLook = Vector3.Normalize(cameraTarget - cameraPosition);

            this.bUpdateView = true;
            this.bUpdateProjection = true;
            
        }

        protected virtual void updateProjection()
        {
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(fieldOfView),
                                        aspectRatio, nearClipPlane, farClipPlane);
            bUpdateProjection = false;

            //projectionMatrix changes so the frustum changes too
            updateFrustum();
        }

        protected virtual void updateView()
        {
           
            //cameraUp.Normalize();
            //cameraRight = Vector3.Normalize(Vector3.Cross(cameraLook, cameraUp));
            //viewMatrix = Matrix.CreateLookAt(cameraPosition, cameraPosition + cameraLook, cameraUp);
            //bUpdateView = false;

            ////viewMatrix changes so the frustum changes too
            //updateFrustum();  
       }

        public virtual void Reset()
        {
        }
        protected virtual void updateFrustum()
        {
            boundingFrustum = new BoundingFrustum(viewMatrix * projectionMatrix);
        }

        public abstract void Update(GameTime time);

        public static bool CollisionIntersects(BoundingBox box, Vector3 newPosition)
        {
            return new BoundingSphere(newPosition, CameraBehaviour.CAMERA_BOUNDINGSPHERE_RADIUS).Intersects(box);
        }

        public static bool CollisionIntersects(BoundingSphere sphere, Vector3 newPosition)
        {
            return new BoundingSphere(newPosition, CameraBehaviour.CAMERA_BOUNDINGSPHERE_RADIUS).Intersects(sphere);
        }

        public virtual bool FrustumContains(ref BoundingBox box)
        {
            //if contains == ContainmentType.Disjoint then object is outside the frustum, so return false
            //if containts == ContainmentType.Contains or ContainmentType.Intersect then object is inside or on border of frustum, so return true
            return ((this.boundingFrustum.Contains(box) != ContainmentType.Disjoint) ? true : false);
        }

        public virtual bool FrustumContains(ref BoundingSphere sphere)
        {
            //if contains == ContainmentType.Disjoint then object is outside the frustum, so return false
            //if containts == ContainmentType.Contains or ContainmentType.Intersect then object is inside or on border of frustum, so return true
            return ((this.boundingFrustum.Contains(sphere) != ContainmentType.Disjoint) ? true : false);
        }

        //generates a ray - a ray connects two points in space
        //point A: nearsource - mouse's position when unprojected
        //onto the near clipping plane (i.e. where z=0) 
        //point B: farsource - mouse's position when unprojected onto the
        //far clipping plane (i.e. where z = 1)
        public Ray GetPickRay(Vector2 mousePosition, Viewport viewPort)
        {
            //near clipping plane position
            Vector3 nearsource = new Vector3(mousePosition, 0);
            //far clipping plane position
            Vector3 farsource = new Vector3(mousePosition, 1);

            //obtain positions of these points in world space
            //why do we convert positions into world space?
            //this is because EVERYTHING we see or hear is defined in world space
            Vector3 nearPoint = viewPort.Unproject(nearsource, Projection, View, Matrix.Identity);
            Vector3 farPoint = viewPort.Unproject(farsource, Projection, View, Matrix.Identity);
            
            //calculate a vector that joins these two points
            Vector3 direction = farPoint - nearPoint;
            direction.Normalize();

            // return a ray from the near clip plane to the far clip plane.
            return new Ray(nearPoint, direction);
        }
    }
}