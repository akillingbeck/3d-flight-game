﻿using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class CameraBehaviour
    {
        #region CAMERA;
        public static float CAMERA_BOUNDINGSPHERE_RADIUS = 0.75f;
        #endregion;

        #region PROJECTION_PROPERTIES;
        public static float FOV_IN_DEGREES = 45; //degrees
        public static float ASPECT_RATIO = 4.0f/3;
        public static float NEAR_CLIP_PLANE = 0.5f; //how does the near clip plane value affect bounding sphere radius??? set them equal and then collide with a box
        public static float FAR_CLIP_PLANE = 20000f;
        #endregion;

        #region FIRSTPERSONCAMERA;

        public static float FIRSTPERSON_HEAD_HEIGHT = 3f;

        public static Keys FIRSTPERSON_FORWARD_KEY = Keys.W;
        public static Keys FIRSTPERSON_BACKWARD_KEY = Keys.S;
        public static Keys FIRSTPERSON_LEFT_KEY = Keys.A;
        public static Keys FIRSTPERSON_RIGHT_KEY = Keys.D;
        public static Keys FIRSTPERSON_JUMP_KEY = Keys.X;
        public static Keys FIRSTPERSON_CROUCH_KEY = Keys.C;
        public static Keys FIRSTPERSON_PRONE_KEY = Keys.V;

        public static float FIRSTPERSON_WALK_FORWARD_SPEED = 50f;
        public static float FIRSTPERSON_WALK_BACKWARD_SPEED = 25f;
        public static float FIRSTPERSON_STRAFE_SPEED = 10f;
        public static float FIRSTPERSON_YAW_ANGLE = MathHelper.PiOver4;
        #endregion;

        #region SECURITYCAMERA;
        public static float SECURITY_SWEEP_ANGLE_INCREMENT = 0.05f;
        public static float SECURITY_MAX_SWEEP_ANGLE = 5f;
        public static int SECURITY_SWEEP_RATE = 5;
        #endregion;

        

    }
}
