﻿using Microsoft.Xna.Framework;

namespace GameLibrary
{
    //stores a path for the camera
    public class RailCameraPath
    {
        protected CurveLoopType curveType;
        private Curve3D cameraPath;
        private Curve3D cameraLookAtPath;

        public RailCameraPath(CurveLoopType curveType)
        {
            this.curveType = curveType;
            this.cameraPath = new Curve3D(curveType);
            this.cameraLookAtPath = new Curve3D(curveType);
        }
      
        public void Add(RailCameraPosition position)
        {
            float time = position.getElapsedTime();
            cameraPath.AddPoint(position.getPosition(), time);
            cameraLookAtPath.AddPoint(position.getLook(), time);
        }
       
        public Vector3 getPosition(float elapsedTime)
        {
            //set the position of the camera
            return cameraPath.GetPointOnCurve(elapsedTime);      
        }

        public Vector3 getLook(float elapsedTime)
        {
            //set the look of the camera
            return cameraLookAtPath.GetPointOnCurve(elapsedTime);
        }
    }
}
