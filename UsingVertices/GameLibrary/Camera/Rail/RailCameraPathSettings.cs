﻿using Microsoft.Xna.Framework;

namespace GameLibrary
{
    //stores different camera movements
    public class RailCameraPathSettings
    {
        public static RailCameraPath getSlidePath(CurveLoopType curveLoopType)
        {
            // the more points you add to the path - the smoother the camera movement
            RailCameraPath cameraPath = new RailCameraPath(curveLoopType);
            float time = 0;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 10, 25), new Vector3(0, 0, -1), time));
            time += 1000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 10, 18), new Vector3(0, 0, -1), time));
            time += 1000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 8, 15), new Vector3(0, -2, -1), time));
            time += 2000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 6, 10), new Vector3(0, -1, -1), time));
            time += 2000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 4, 5), new Vector3(0, 0, -1), time));
            return cameraPath;
        }

        public static RailCameraPath getVerticalDropPath(CurveLoopType curveLoopType)
        {
            // the more points you add to the path - the smoother the camera movement
            RailCameraPath cameraPath = new RailCameraPath(curveLoopType);
            float time = 0;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 5, 0), new Vector3(0, -1, 0), time));
            time += 4000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 120, 0), new Vector3(0, -1, 0), time));
            time += 1000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 120, 0), new Vector3(0, -1, 0), time));
            time += 1000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 5, 0), new Vector3(0, -1, 0), time));
            time += 1000;
            cameraPath.Add(new RailCameraPosition(new Vector3(0, 5, 0), new Vector3(0, -1, 0), time));
            return cameraPath;
        }

    }
}
