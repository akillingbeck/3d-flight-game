﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameLibrary
{
    //moves along a rail defined by RailCameraPathSettings
    public class RailCamera : Camera
    {
        private float elapsedTime; //used to access camera look and position on the curve
        protected RailCameraPath railCameraPath;

        public RailCamera(RailCameraPath railCameraPath, Vector3 Up)
            //position the camera anywhere since it will follow the rail anyway
            : base(new Vector3(0, 1, 1), new Vector3(0, 1, 0), Up)
        {
            this.railCameraPath = railCameraPath;
        }

        public override void Update(GameTime gameTime)
        {
            
            elapsedTime += (float)gameTime.ElapsedGameTime.Milliseconds;
            //resets the camera back to its first position
            if (game.keyboardManager.isKeyPressed(Keys.R))
            {
                elapsedTime = 0;
            }

            //set camera position & look
            Position = railCameraPath.getPosition(elapsedTime);
            Look = railCameraPath.getLook(elapsedTime);
             
        }
    }
}