﻿using Microsoft.Xna.Framework;

namespace GameLibrary
{
    //stores where the camera should be, what it is looking towards, and when it was there
    public class RailCameraPosition
    {
        protected Vector3 position;
        protected Vector3 look;
        protected float elapsedTime;

        public RailCameraPosition(Vector3 position, Vector3 look, float elapsedTime)
        {
            this.position = position;
            this.look = look;
            this.elapsedTime = elapsedTime;
        }

        public Vector3 getPosition()
        {
            return position;
        }
        public Vector3 getLook()
        {
            return look;
        }
        public float getElapsedTime()
        {
            return elapsedTime;
        }
    }
}
