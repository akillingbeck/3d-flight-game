using Microsoft.Xna.Framework;


namespace GameLibrary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class SecurityCamera : Camera
    {
        protected float maxSweepAngle;
        protected int direction = 1;
        protected float currentSweep = 0;
        protected int sweepRate = 0;

        public SecurityCamera(Vector3 pos, Vector3 target, Vector3 up)
            : base(pos, target, up)
        {
        }

        public override void Update(GameTime gameTime)
        {
            if ((currentSweep >= CameraBehaviour.SECURITY_MAX_SWEEP_ANGLE)
                || (currentSweep <= -CameraBehaviour.SECURITY_MAX_SWEEP_ANGLE))
            {
                direction = -1*direction;
            }
         
            currentSweep += direction*CameraBehaviour.SECURITY_SWEEP_ANGLE_INCREMENT;
            
            if (sweepRate == CameraBehaviour.SECURITY_SWEEP_RATE)
            {
                Look = Vector3.Transform(Look,
                        Matrix.CreateFromAxisAngle(Up, direction * CameraBehaviour.SECURITY_SWEEP_ANGLE_INCREMENT));
                sweepRate = 0;
            }
            else
            {
                sweepRate++;
            }

            updateView();
        }
    }
}