﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class WireframeBoxVB : Object3D
    {
        //because every box position can dimension can be different we must use a separate vertex and index buffer for each box
        protected VertexPositionColor[] verts; //stores points and associated info.
        protected VertexBuffer vertsBuffer;    //memory location on the graphics card
        protected short[] vertsIndices;        //the indice-vertex data
        protected IndexBuffer indexBuffer;     //pointer to indices to use when drawing

        public WireframeBoxVB(string name)
            : base(name,Vector3.Zero,Vector3.One,Vector3.One,Vector3.One)
        {
            InitialiseVertices();
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        protected void InitialiseVertices()
        {
            verts = new VertexPositionColor[4];
            //top left
            verts[0] = new VertexPositionColor(new Vector3(-1, 1, 0), Color.Red);
            //top right
            verts[1] = new VertexPositionColor(new Vector3(1, 1, 0), Color.Red);
            //bottom right
            verts[2] = new VertexPositionColor(new Vector3(1, -1, 0), Color.Red);
            //bottom left
            verts[3] = new VertexPositionColor(new Vector3(-1, -1, 0), Color.Red);

            vertsIndices = new short[10];
            //horizontal top
            vertsIndices[0] = 0; 
            vertsIndices[1] = 1; 
            //vertical right
            vertsIndices[2] = 1; 
            vertsIndices[3] = 2; 
            //horizontal bottom
            vertsIndices[4] = 2; 
            vertsIndices[5] = 3;
            //vertical left
            vertsIndices[6] = 3;
            vertsIndices[7] = 0; 
            //diagonal
            vertsIndices[8] = 0;
            vertsIndices[9] = 2; 

            //reserve space on VRAM of GFX for verts buffer
            vertsBuffer = new VertexBuffer(game.GraphicsDevice,
                typeof(VertexPositionColor), verts.Length, BufferUsage.WriteOnly);
            //reserve space on VRAM of GFX for indices
            indexBuffer = new IndexBuffer(game.GraphicsDevice,
                typeof(short), vertsIndices.Length, BufferUsage.WriteOnly);

            //load the vertex information on GFX
            vertsBuffer.SetData(verts, 0, verts.Length); //may use subset of contents
            //load the index information on GFX
            indexBuffer.SetData(vertsIndices); //always use all contents
        }
        public override void Update(GameTime gameTime)
        {        }
        public override void Draw(GameTime gameTime)
        {
            //this positions the primitive on-screen by applying a transformation INSIDE GFX card
            game.wireframeEffect.World = world;

            //apply the default technique and first pass within the technique (apply HLSL code)
            game.wireframeEffect.CurrentTechnique.Passes[0].Apply();
            //in this draw use the vertex buffer (i.e. data on VRAM)
            game.GraphicsDevice.SetVertexBuffer(vertsBuffer);
            //in this draw use the index buffer (i.e. data on VRAM)
            game.GraphicsDevice.Indices = indexBuffer;
            //LineList draws pairs of points - may or may not be connected
            //LineStrip connects all points in the vertsBuffer
            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.LineList,
                0, 0, 4, 0, 6);


        }
    }
}