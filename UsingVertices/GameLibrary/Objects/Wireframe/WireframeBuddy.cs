using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
    //uses a vertex buffer to draw the XYZ axes
    public class WireframeBuddy : Object3D
    {
        protected VertexPositionColor[] verts;
        protected VertexBuffer vertsBuffer;

        public Color axisColor { get; protected set; }
        public Color textColor { get; protected set; }

        public WireframeBuddy(string name,  Color axisColor, Color textColor)
            : base(name,Vector3.Zero,Vector3.One,Vector3.One,Vector3.One)
        {
            this.world *= world;
            this.axisColor = axisColor;
            this.textColor = textColor;
            InitialiseVertices();  
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
        }

        protected void InitialiseVertices()
        {
  
            verts = new VertexPositionColor[20];

            //x-axis
            verts[0] = new VertexPositionColor(new Vector3(-1, 0, 0), axisColor);
            verts[1] = new VertexPositionColor(new Vector3(1, 0, 0), axisColor);
            //y-axis
            verts[2] = new VertexPositionColor(new Vector3(0, -1, 0), axisColor);
            verts[3] = new VertexPositionColor(new Vector3(0, 1, 0), axisColor);
            //z-axis
            verts[4] = new VertexPositionColor(new Vector3(0, 0, -1), axisColor);
            verts[5] = new VertexPositionColor(new Vector3(0, 0, 1), axisColor);

            //x on screen
            verts[6] = new VertexPositionColor(new Vector3(1.125f, -0.125f, 0), textColor);
            verts[7] = new VertexPositionColor(new Vector3(1.25f, 0.125f, 0), textColor);
            verts[8] = new VertexPositionColor(new Vector3(1.125f, 0.125f, 0), textColor);
            verts[9] = new VertexPositionColor(new Vector3(1.25f, -0.125f, 0), textColor);

            //y on screen
            verts[10] = new VertexPositionColor(new Vector3(-0.125f, 1.125f, 0), textColor);
            verts[11] = new VertexPositionColor(new Vector3(0.125f, 1.25f, 0), textColor);
            verts[12] = new VertexPositionColor(new Vector3(0, 1.1875f, 0), textColor);
            verts[13] = new VertexPositionColor(new Vector3(-0.125f, 1.25f, 0), textColor);

            //z on screen
            verts[14] = new VertexPositionColor(new Vector3(0, 0.125f, 1.25f), textColor);
            verts[15] = new VertexPositionColor(new Vector3(0, 0.125f, 1.125f), textColor);

            verts[16] = new VertexPositionColor(new Vector3(0, 0.125f, 1.125f), textColor);
            verts[17] = new VertexPositionColor(new Vector3(0, -0.125f, 1.25f), textColor);

            verts[18] = new VertexPositionColor(new Vector3(0, -0.125f, 1.25f), textColor);
            verts[19] = new VertexPositionColor(new Vector3(0, -0.125f, 1.125f), textColor);

            vertsBuffer = new VertexBuffer(game.GraphicsDevice, typeof(VertexPositionColor), verts.Length, BufferUsage.WriteOnly); 
            vertsBuffer.SetData(verts, 0, verts.Length);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            this.world = Matrix.Identity * Matrix.CreateScale(50f) * Matrix.CreateTranslation(Vector3.One);

            base.Update(gameTime);
        }


          /// <summary>
        /// Allows the game component to draw.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            //set properties
            game.wireframeEffect.World = world;
            game.wireframeEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(vertsBuffer);
            game.GraphicsDevice.DrawPrimitives(PrimitiveType.LineList, 0, verts.Length / 2);
            base.Draw(gameTime);
        }
   }
}
