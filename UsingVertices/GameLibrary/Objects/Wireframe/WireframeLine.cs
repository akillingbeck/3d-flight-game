using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


namespace GameLibrary
{
    /*
     * Function:    This class draws a user defined line with each end colorised to allow user to orient the box easily.
     *              This is because each line will have different dimensions and so sharing common dimensions (i.e. in the vertex buffer) is pointless.          
     * Author:      NMCG
     * Date:        1/2012
     * Version:     1.0
     * Revisions:   None
     */
    public class WireframeLine : Object3D
    {
        //because every box position can dimension can be different we must use a separate vertex and index buffer for each box
        protected VertexPositionColor[] verts;

        //<-
        protected Vector3 centre, start, end;
        protected Color startColor, endColor;

        public WireframeLine(string name, Vector3 start, Vector3 end, Color startColor, Color endColor)
            : base(name, Vector3.Zero,Vector3.One,Vector3.One,Vector3.One)
        {
            this.start = start;
            this.end = end;

            this.startColor = startColor;
            this.endColor = endColor;

            InitialiseVertices();
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected void InitialiseVertices()
        {
            verts = new VertexPositionColor[2];
            verts[0] = new VertexPositionColor(start, startColor);  //start
            verts[1] = new VertexPositionColor(end, endColor);  //end
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            //base.Update(gameTime); //Don't need this line since Object3D::Update() does nothing.
        }

        /// <summary>
        /// Allows the game component to draw.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            game.wireframeEffect.World = world;

            game.wireframeEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, verts, 0, 1);

            //base.Draw(gameTime); //Don't need this line since Object3D::Draw() does nothing.
        }
    }
}