using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


namespace GameLibrary
{
    /*
     * Function:    This class draws a user defined box with each side colorised to allow user to orient the box easily.
     *              Note that the indexbuffer and vertexbuffer are defined inside the class unlike
     *              the WireframeCube where a common WireframeCubeParams class is used to hold index and vertex buffers which are static.
     *              This is because each cube will have different dimensions and so sharing common dimensions (i.e. in the vertex buffer) is pointless.          
     * Author:      NMCG
     * Date:        1/2012
     * Version:     1.0
     * Revisions:   None
     */

    public class WireframeBox : Object3D
    {
        //because every box position can dimension can be different we must use a separate vertex and index buffer for each box
        protected VertexPositionColor[] verts;
        protected short[] vertsIndices;
        public IndexBuffer indexBuffer;
        public VertexBuffer vertsBuffer;
  
        //<-
        protected Vector3 center, bottomLeft, topRight;
        protected Color leftColor, rightColor;

        public WireframeBox(string name, Vector3 bottomLeft, Vector3 topRight, Color leftColor, Color rightColor)
            : base(name,Vector3.Zero,Vector3.One,bottomLeft,topRight)
        {
            this.bottomLeft = bottomLeft;
            this.topRight = topRight;

            this.leftColor = leftColor;
            this.rightColor = rightColor;

            this.center = (bottomLeft + topRight)/2;

            InitialiseVertices();
        }


        public override void Initialize()
        {
            base.Initialize();
        }

        protected void InitialiseVertices()
        {
            //set the static verts used by all cubes
            verts = new VertexPositionColor[8];

            verts[0] = new VertexPositionColor(bottomLeft, leftColor);  //bottom back left
            verts[1] = new VertexPositionColor(new Vector3(topRight.X, bottomLeft.Y, bottomLeft.Z), rightColor);  //bottom back right

            verts[2] = new VertexPositionColor(new Vector3(topRight.X, bottomLeft.Y, topRight.Z), rightColor);  //bottom front right
            verts[3] = new VertexPositionColor(new Vector3(bottomLeft.X, bottomLeft.Y, topRight.Z), leftColor);   //bottom front left

            verts[4] = new VertexPositionColor(new Vector3(bottomLeft.X, topRight.Y, bottomLeft.Z), leftColor);  //top back left
            verts[5] = new VertexPositionColor(new Vector3(topRight.X, topRight.Y, bottomLeft.Z), rightColor);  //top back right

            verts[6] = new VertexPositionColor(topRight, rightColor);  //top front right
            verts[7] = new VertexPositionColor(new Vector3(bottomLeft.X, topRight.Y, topRight.Z), leftColor);   //top front left

            vertsIndices = new short[24];

            //bottom face
            vertsIndices[0] = 0;    //bottom back line
            vertsIndices[1] = 1;

            vertsIndices[2] = 1;    //bottom right line
            vertsIndices[3] = 2;

            vertsIndices[4] = 2;    //bottom right line
            vertsIndices[5] = 3;

            vertsIndices[6] = 3;    //bottom right line
            vertsIndices[7] = 0;

            //top face
            vertsIndices[8] = 4;    //top back line
            vertsIndices[9] = 5;

            vertsIndices[10] = 5;    //top right line
            vertsIndices[11] = 6;

            vertsIndices[12] = 6;    //top right line
            vertsIndices[13] = 7;

            vertsIndices[14] = 7;    //top right line
            vertsIndices[15] = 4;

            //side edges 
            vertsIndices[16] = 0;   //back left vertical line
            vertsIndices[17] = 4;

            vertsIndices[18] = 1;   //back right vertical line
            vertsIndices[19] = 5;

            vertsIndices[20] = 2;   //front right vertical line
            vertsIndices[21] = 6;

            vertsIndices[22] = 3;   //front left vertical line
            vertsIndices[23] = 7;

            vertsBuffer = new VertexBuffer(game.GraphicsDevice, typeof(VertexPositionColor), verts.Length, BufferUsage.WriteOnly);
            vertsBuffer.SetData(verts, 0, verts.Length);

            indexBuffer = new IndexBuffer(game.GraphicsDevice, typeof(short), vertsIndices.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData<short>(vertsIndices);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            //base.Update(gameTime); //Don't need this line since Object3D::Update() does nothing.
        }

        /// <summary>
        /// Allows the game component to draw.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.wireframeEffect.World = world;

            game.wireframeEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(vertsBuffer);
            game.GraphicsDevice.Indices = indexBuffer;
            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.LineList, 0, 0, 8, 0, 12);

            //base.Draw(gameTime); //Don't need this line since Object3D::Draw() does nothing.
        }
    }
}