﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class WireframeQuad : Object3D
    {
        //because every box position can dimension can be different we must use a separate vertex and index buffer for each box
        protected VertexPositionColor[] verts; //stores points and associated info.
        protected VertexBuffer vertsBuffer;    //memory location on the graphics card
        protected short[] vertsIndices;        //the indice-vertex data
        protected IndexBuffer indexBuffer;     //pointer to indices to use when drawing

        public WireframeQuad(string name, Vector3 scale, Color color)
            : base(name,Vector3.Zero, scale, new Vector3(-1, -1, 0), new Vector3(1, 1, 0))
        {
            InitialiseVertices();
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        protected void InitialiseVertices()
        {
            verts = new VertexPositionColor[4];
            //top left
            verts[0] = new VertexPositionColor(new Vector3(-1, 1, 0), Color.Red);
            //top right
            verts[1] = new VertexPositionColor(new Vector3(1, 1, 0), Color.Red);
            //bottom right
            verts[2] = new VertexPositionColor(new Vector3(1, -1, 0), Color.Red);
            //bottom left
            verts[3] = new VertexPositionColor(new Vector3(-1, -1, 0), Color.Red);

            vertsIndices = new short[6];

            vertsIndices[0] = 0; //right
            vertsIndices[1] = 1; //right
            vertsIndices[2] = 2; //down
            vertsIndices[3] = 3; //left
            vertsIndices[4] = 0; //up
            vertsIndices[5] = 2; //diagonal
        
            //reserve space on VRAM of GFX for verts buffer
            vertsBuffer = new VertexBuffer(game.GraphicsDevice,
                typeof(VertexPositionColor), verts.Length, BufferUsage.WriteOnly);
            //reserve space on VRAM of GFX for indices
            indexBuffer = new IndexBuffer(game.GraphicsDevice,
                typeof(short), vertsIndices.Length, BufferUsage.WriteOnly);

            //load the vertex information on GFX
            vertsBuffer.SetData(verts, 0, verts.Length); //may use subset of contents
            //load the index information on GFX
            indexBuffer.SetData(vertsIndices); //always use all contents
        }
        public override void Update(GameTime gameTime)
        {        
        }
        public override void Draw(GameTime gameTime)
        {
            //this positions the primitive on-screen by applying a transformation INSIDE GFX card
            game.wireframeEffect.World = world;

            //apply the default technique and first pass within the technique (apply HLSL code)
            game.wireframeEffect.CurrentTechnique.Passes[0].Apply();
            //in this draw use the vertex buffer (i.e. data on VRAM)
            game.GraphicsDevice.SetVertexBuffer(vertsBuffer);
            //in this draw use the index buffer (i.e. data on VRAM)
            game.GraphicsDevice.Indices = indexBuffer;
            //LineList draws pairs of points - may or may not be connected
            //LineStrip connects all points in the vertsBuffer
            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.LineStrip, 0, 0, 4, 0, 5);


        }
    }
}