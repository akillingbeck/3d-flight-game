﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    
    public class ArmyBarracks : Object3D
    {

        Vector3 leftWallPosition;
        Vector3 rightWallPosition;
        Vector3 backWallPosition;
        Vector3 roofPosition;
        Matrix leftWallMat;
        Matrix rightWallMat;
        Matrix backWallMat;
        Matrix roofMat;
        public TextureQuad leftWall,rightWall,backWall,roof;
        public ArmyBarracks(Vector3 Position,Texture2D wallTextures,float scale):base("Barracks",Position,Vector3.One,
            new Vector3(-1,-1,0),new Vector3(1,1,0))
        {
            leftWallPosition = Position + new Vector3(-scale, 0, 0);
            rightWallPosition = Position + new Vector3(scale, 0, 0);
            backWallPosition = Position + new Vector3(0, 0,-scale);
            roofPosition = Position + new Vector3(0, scale, 0);
            Random rand = new Random();

            int randAngle = rand.Next(0,91);
            leftWall = new TextureQuad("BarracksLeftWall", Matrix.CreateScale(scale)*Matrix.CreateRotationY(MathHelper.ToRadians(90))*Matrix.CreateTranslation(leftWallPosition), Color.White, 1, wallTextures);
            rightWall = new TextureQuad("BarracksRightWall", Matrix.CreateScale(scale) * Matrix.CreateRotationY(MathHelper.ToRadians(90)) * Matrix.CreateTranslation(rightWallPosition), Color.White, 1, wallTextures);
            backWall = new TextureQuad("BarracksBackWall", Matrix.CreateScale(scale) * Matrix.CreateTranslation(backWallPosition), Color.White, 1, wallTextures);
            roof = new TextureQuad("BarracksRoof", Matrix.CreateScale(scale) * Matrix.CreateRotationX(MathHelper.ToRadians(90)) * Matrix.CreateTranslation(roofPosition), Color.White, 1, wallTextures);
        }
        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }

    }
}
