﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
/*
 * Author:      NMCG
 * Purpose:     Uses a custom effect to draw the terrain with sand, snow, earth etc
 * Date:        28/2/12
 * Revisions:   UNDER CONSTRUCTION!
 */
namespace GameLibrary
{
    public class MultiTextureTerrain : Terrain
    {
        protected Texture2D[] terrainTextureArray;
        protected Effect effect;
        protected string effectTechnique;
        protected Vector4 lightPosition, lightColor;

        public MultiTextureTerrain(string name, Matrix world, Effect effect, 
                        string effectTechnique, Texture2D heightMap, 
                                    Texture2D[] terrainTextureArray, Vector3 scaleFactor,
                                                Vector4 lightPosition, Vector4 lightColor)
        : base(name,world, heightMap, null/*pass null for texture*/, scaleFactor)
        {
            //this stores an array of textures - typically 4 - snow, sand, earth, rock
            this.terrainTextureArray = terrainTextureArray;
            //custom effect used to draw the terrain
            this.effect = effect;
            //specifies what technique to apply
            this.effectTechnique = effectTechnique;
            //set light attributes
            this.lightPosition = lightPosition;
            this.lightColor = lightColor;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }


        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
