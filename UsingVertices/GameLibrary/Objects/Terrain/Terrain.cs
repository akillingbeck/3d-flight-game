﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

/*
 * Source:      XNA 3.0 Game Programming Recipes
 * Chapter:     5
 * Amended by:  NMCG - 14/4/11
 */

using UsingVertices;

namespace GameLibrary
{
    public class Terrain : Object3D
    {
        protected BasicEffect basicEffect;
        //texture to apply to terrain
        protected Texture2D terrainTexture;
        //stores height data from the map
        protected float[,] heightData;
        //actual Texture2D containing height map data
        protected Texture2D heightMap;

        //stores map dimensions
        protected int width, height;
        //used to increase peak to trough map amplitude
        protected Vector3 scaleFactor = new Vector3(25.0f,25.0f,25.0f);

        protected VertexBuffer terrainVertexBuffer;
        protected IndexBuffer terrainIndexBuffer;

        public Terrain(string name,Matrix world, Texture2D heightMap, Texture2D terrainTexture, Vector3 scaleFactor)
            : base(name,Vector3.Zero,scaleFactor,Vector3.One,Vector3.One)
        {
            this.heightMap = heightMap;
            this.world = world;
            this.terrainTexture = terrainTexture;
            this.basicEffect = new BasicEffect(game.GraphicsDevice);
            this.scaleFactor = scaleFactor;

            //set width and height used for index and vertex buffers
            this.width = heightMap.Width;
            this.height = heightMap.Height;
    
            LoadHeightData(heightMap); //load height map texture directly - so we dont store it as member variable

            //set up vertices, indices, normals and buffers
            VertexPositionNormalTexture[] terrainVertices = CreateTerrainVertices();
            int[] terrainIndices = CreateTerrainIndices();
            terrainVertices = GenerateNormalsForTriangleStrip(terrainVertices, terrainIndices);
            CreateBuffers(terrainVertices, terrainIndices);
        }

        private void LoadHeightData(Texture2D heightMap)
        {
            float minimumHeight = 255;
            float maximumHeight = 0;

            //Interesting its a 1-d array
            Color[] heightMapColors = new Color[width * height];
            //we use a 1-d array because GetData() doesnt exist for 2d arrays
            heightMap.GetData<Color>(heightMapColors);

            //converting the 1-d into a 2-d array
            //stripping out ONLY the red channel data from the color array
            heightData = new float[width, height];
            //output is a 2d array and we also have max an min values
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    heightData[x, y] = heightMapColors[x + y * width].R;
                    if (heightData[x, y] < minimumHeight) minimumHeight = heightData[x, y];
                    if (heightData[x, y] > maximumHeight) maximumHeight = heightData[x, y];
                }
            }
            //stretch these values so that they occupy the entire range 0-255
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    heightData[x, y] = (heightData[x, y] - minimumHeight) / (maximumHeight - minimumHeight) * scaleFactor.X;
        }

        private VertexPositionNormalTexture[] CreateTerrainVertices()
        {
            VertexPositionNormalTexture[] terrainVertices = new VertexPositionNormalTexture[width * height];

            int i = 0;
            for (int z = 0; z < height; z++)
            {
                for (int x = 0; x < width; x++)
                {
                    Vector3 position = new Vector3(x, heightData[x, z], -z);
                    Vector3 normal = new Vector3(0, 0, 1);
                    Vector2 texCoord = new Vector2((float)x / scaleFactor.X, (float)z / scaleFactor.X);

                    terrainVertices[i++] = new VertexPositionNormalTexture(position, normal, texCoord);
                }
            }

            return terrainVertices;
        }

        private int[] CreateTerrainIndices()
        {
            int[] terrainIndices = new int[(width) * 2 * (height - 1)];

            int i = 0;
            int z = 0;
            while (z < height - 1)
            {
                for (int x = 0; x < width; x++)
                {
                    terrainIndices[i++] = x + z * width;
                    terrainIndices[i++] = x + (z + 1) * width;
                }
                z++;

                if (z < height - 1)
                {
                    for (int x = width - 1; x >= 0; x--)
                    {
                        terrainIndices[i++] = x + (z + 1) * width;
                        terrainIndices[i++] = x + z * width;
                    }
                }
                z++;
            }

            return terrainIndices;
        }

        private VertexPositionNormalTexture[] GenerateNormalsForTriangleStrip(VertexPositionNormalTexture[] vertices, int[] indices)
        {
            for (int i = 0; i < vertices.Length; i++)
                vertices[i].Normal = new Vector3(0, 0, 0);

            bool swappedWinding = false;
            for (int i = 2; i < indices.Length; i++)
            {
                Vector3 firstVec = vertices[indices[i - 1]].Position - vertices[indices[i]].Position;
                Vector3 secondVec = vertices[indices[i - 2]].Position - vertices[indices[i]].Position;
                Vector3 normal = Vector3.Cross(firstVec, secondVec);
                normal.Normalize();

                if (swappedWinding)
                    normal *= -1;

                if (!float.IsNaN(normal.X))
                {
                    vertices[indices[i]].Normal += normal;
                    vertices[indices[i - 1]].Normal += normal;
                    vertices[indices[i - 2]].Normal += normal;
                }

                swappedWinding = !swappedWinding;
            }

            for (int i = 0; i < vertices.Length; i++)
                vertices[i].Normal.Normalize();

            return vertices;
        }

        private void CreateBuffers(VertexPositionNormalTexture[] vertices, int[] indices)
        {
            terrainVertexBuffer = new VertexBuffer(game.GraphicsDevice,
               typeof(VertexPositionNormalTexture), vertices.Length, BufferUsage.WriteOnly);
            terrainVertexBuffer.SetData(vertices);

            terrainIndexBuffer = new IndexBuffer(game.GraphicsDevice,
                typeof(int), indices.Length, BufferUsage.WriteOnly);
            terrainIndexBuffer.SetData(indices);
        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw(GameTime gameTime)
        {
            //draw terrain
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            basicEffect.World = world;
            basicEffect.View = game.cameraManager.ActiveCamera.View;
          
            basicEffect.Projection = game.cameraManager.ActiveCamera.Projection;
            basicEffect.Texture = terrainTexture;
            basicEffect.TextureEnabled = true;

         //   basicEffect.EnableDefaultLighting();
            //Vector3 lightDirection = new Vector3(1, -1, 1);
            //lightDirection.Normalize();
            //basicEffect.DirectionalLight0.Direction = lightDirection;
            //basicEffect.DirectionalLight0.Enabled = true;
            //basicEffect.AmbientLightColor = new Vector3(0.3f, 0.3f, 0.3f);
            //basicEffect.DiffuseColor = new Vector3(0.9f, 0.9f, 0.3f);
            //basicEffect.DirectionalLight1.Enabled = false;
            //basicEffect.DirectionalLight2.Enabled = false;
            //basicEffect.SpecularColor = new Vector3(0, 0, 0);

            basicEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(terrainVertexBuffer);
            game.GraphicsDevice.Indices = terrainIndexBuffer;
       
            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleStrip, 0, 0, width * height, 0, width * 2 * (height - 1) - 2);
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.GraphicsDevice.BlendState = BlendState.AlphaBlend;
        }

        public float GetClippedHeightAt(float x, float z)
        {
            bool invalid = x < 0;
            invalid |= z < 0;
            invalid |= x > width - 1;
            invalid |= z > height - 1;
            if (invalid)
                return 10;
            else
                return heightData[(int)x, (int)z];
        }

        public float GetExactHeightAt(float xCoord, float zCoord)
        {
            bool invalid = xCoord < 0;
            invalid |= zCoord < 0;
            invalid |= xCoord > heightData.GetLength(0) - 1;
            invalid |= zCoord > heightData.GetLength(1) - 1;
            if (invalid)
                return 10;

            int xLower = (int)xCoord;
            int xHigher = xLower + 1;
            float xRelative = (xCoord - xLower) / ((float)xHigher - (float)xLower);

            int zLower = (int)zCoord;
            int zHigher = zLower + 1;
            float zRelative = (zCoord - zLower) / ((float)zHigher - (float)zLower);

            float heightLxLz = heightData[xLower, zLower];
            float heightLxHz = heightData[xLower, zHigher];
            float heightHxLz = heightData[xHigher, zLower];
            float heightHxHz = heightData[xHigher, zHigher];

            bool cameraAboveLowerTriangle = (xRelative + zRelative < 1);

            float finalHeight;
            if (cameraAboveLowerTriangle)
            {
                finalHeight = heightLxLz;
                finalHeight += zRelative * (heightLxHz - heightLxLz);
                finalHeight += xRelative * (heightHxLz - heightLxLz);
            }
            else
            {
                finalHeight = heightHxHz;
                finalHeight += (1.0f - zRelative) * (heightHxLz - heightHxHz);
                finalHeight += (1.0f - xRelative) * (heightLxHz - heightHxHz);
            }

            return finalHeight;
        }
    }
}
