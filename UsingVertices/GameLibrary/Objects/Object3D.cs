using Microsoft.Xna.Framework;

using UsingVertices;

namespace GameLibrary
{
    public class Object3D
    {
        //modifying this matrix modifies how the object is drawn on-screen
        //i.e. we can scale, rotate, and translate the object
        protected Matrix world;
        public static Main game;
        protected Matrix billboard;
        protected Matrix rotation;
        public bool rayPicked = false;

        protected Vector3 scale;
        protected Vector3 min;
        protected Vector3 max;

        protected Vector3 Position;

        protected bool updateThis = true;

        protected string name;
        public BoundingSphere boundingSphere;
        protected WireframeBox debugBox;
        public bool bCollideable = false;
        protected bool bCollision = false;
        protected Vector3 centre;
        protected float radius;
        //protected Vector3 minPoint = Vector3.Zero, maxPoint = Vector3.Zero;
        #region PROPERTIES
        public Matrix ROTATION
        {
            get { return rotation; }
            set { rotation = value; }
        }
        public float POSITIONY
        {
            get { return Position.Y; }
            set { Position.Y = value; }
        }
        public Vector3 POSITION
        {
            get { return Position; }
            set { Position = value; }
        }
        public Matrix BillBoard
        {
            get { return billboard; }
            set { billboard = value; }
        }
        public Vector3 SCALE
        {
            get { return scale; }
        }
        public Matrix World
        {
            get
            {
                return world;
            }
            set
            {
                world = value;
            }
        }
        //returns a user-defined id for each object
        public string Name
        {
            get
            {
                return name;
            }
        }

        //returns the translational component of the world matrix i.e. the transform that affects position
        public Vector3 Translation
        {
            get
            {
                return world.Translation;
            }
            set
            {
                world.Translation = value;
            }
        }
        #endregion
        protected bool debugSphereAdded = false;
        public Object3D(string name, Vector3 Position,Vector3 scale,Vector3 minBB,Vector3 maxBB)
        {
            this.scale = scale;
            this.min = minBB * scale;
            this.max = maxBB * scale;
            min.Z = 0;
            max.Z = -1;
            this.Position = Position;
            this.name = name; //id for each object
            this.centre = ((min + max) / 2)*scale;
            this.radius = max.X - centre.X;
            if (radius > 0)
            {
                this.boundingSphere = new BoundingSphere(centre + Position, radius);
            }
            this.rotation = Matrix.Identity;
            billboard = Matrix.Identity;
            game.objectManager.Add(this);
          //  addBoundingSphere();
        }
        protected void addBoundingSphere()
        {
            if(!debugSphereAdded)
            DebugShapeRenderer.AddBoundingSphere(this.boundingSphere, Color.White,10000f);
            debugSphereAdded = true;
        }
        #region USED BY DRIVABLE OBJECTS
        //See MoveableCube and ThirdPersonCamera for use
        public virtual Vector3 getLook()
        {
            return Vector3.Zero; //dummy - will be overwritten in class that overrides this methods
        }
        public virtual Vector3 getUp()
        {
            return Vector3.Zero;  //dummy - will be overwritten in class that overrides this methods
        }
        public virtual Vector3 getRight()
        {
            return Vector3.Zero;  //dummy - will be overwritten in class that overrides this methods
        }
        #endregion


        public virtual void Initialize()
        {
        }
        public virtual void Update(GameTime gameTime)
        {
          
        //    if(!debugSphereAdded)
           // addBoundingSphere();
              
        }
        public virtual void onCollide(Object3D obj)
        {
           

        }
        public virtual void Draw(GameTime gameTime)
        {

        }
    }
}