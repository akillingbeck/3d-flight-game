﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class City
    {

        public City(Vector3 Position,int rows,int columns)
        {
            Random rand = new Random(65536);
            int ran;
            Building b;
            for (int i = 0; i < rows; i++)
            {
                for (int k = 0; k < columns; k++)
                {


                    ran = rand.Next(1, 6);
                    Texture2D texture = null;
                    switch (ran)
                    {
                        case 1: texture = MiscAssets.buildingType1;
                            break;
                        case 2: texture = MiscAssets.buildingType2;
                            break;
                        case 3: texture = MiscAssets.buildingType3;
                            break;
                        case 4: texture = MiscAssets.buildingType4;
                            break;
                        case 5: texture = MiscAssets.buildingType5;
                            break;
                        default: texture = MiscAssets.buildingType1;
                            break;


                    }

                    int ranX = rand.Next(30, 70);
                    int ranY = rand.Next(130, 370);
                    int ranZ = rand.Next(30, 70);
                    b = new Building(new Vector3(Position.X + (i * 200 + ranX), 0, Position.Z + (k * 200 + ranZ)), texture, new Vector3(ranX, ranY, ranZ));
                }

            }
    

        }
    }
}
