﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace GameLibrary
{
    public class TextureQuad : Object3D
    {
        protected BoundingBox boundingBox;
        protected VertexPositionColorTexture[] verts;
        protected Color color;
        protected Texture2D texture;
        protected int tile;
        public TextureQuad(string name,Matrix world, Color color, int tile, Texture2D texture)
            : base(name,Vector3.Zero,Vector3.One,Vector3.One,Vector3.One)
        {
            this.tile = tile;
            this.texture = texture;
            this.color = color;
            this.world = world;
            this.ROTATION = Matrix.Identity;
            this.POSITION = world.Translation;
            InitialiseVertices();
        }
        public override void Initialize()
        {
            //homework: modify to use vertex and index buffer
            base.Initialize();
        }
        protected void InitialiseVertices()
        {
            verts = new VertexPositionColorTexture[4];
            verts[0] = new VertexPositionColorTexture(new Vector3(-1, 1, 0), color, new Vector2(0,0));
            verts[1] = new VertexPositionColorTexture(new Vector3(1, 1, 0), color, new Vector2(tile,0));
            verts[2] = new VertexPositionColorTexture(new Vector3(-1, -1, 0), color, new Vector2(0, tile));
            verts[3] = new VertexPositionColorTexture(new Vector3(1, -1, 0), color, new Vector2(tile, tile));
        }
        public override void Update(GameTime gameTime)
        {
           
        }
        public override void Draw(GameTime gameTime)
        {
            game.textureEffect.World = world;
            game.textureEffect.Texture = texture;
            game.textureEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>(
                PrimitiveType.TriangleStrip, verts, 0, 2);
        }
    }
}