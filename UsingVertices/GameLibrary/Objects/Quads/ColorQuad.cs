﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace GameLibrary
{
    public class ColorQuad : Object3D
    {
        protected VertexPositionColor[] verts;
        protected Color color;
        public ColorQuad(string name,Vector3 scale, Color color)
            : base(name,Vector3.Zero,scale,new Vector3(-1,-1,0),new Vector3(1,1,0))
        {
            this.color = color;
            InitialiseVertices();
        }
        public override void Initialize()
        {
            //homework: modify to use vertex and index buffer
            base.Initialize();
        }
        protected void InitialiseVertices()
        {
            verts = new VertexPositionColor[4];
            verts[0] = new VertexPositionColor(new Vector3(-1, 1, 0), color);
            verts[1] = new VertexPositionColor(new Vector3(1, 1, 0), color);
            verts[2] = new VertexPositionColor(new Vector3(-1, -1, 0), color);
            verts[3] = new VertexPositionColor(new Vector3(1, -1, 0), color);
        }
        public override void Update(GameTime gameTime)
        {}
        public override void Draw(GameTime gameTime)
        {
            game.wireframeEffect.World = world;
            game.wireframeEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(
                PrimitiveType.TriangleStrip, verts, 0, 2);
        }
    }
}