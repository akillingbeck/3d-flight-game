﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UsingVertices;

namespace GameLibrary
{
    public class SkyDomeModel : BasicModel
    {
        public SkyDomeModel(string name, Vector3 scale, Vector3 rotation, Vector3 translation, Model model, Texture2D texture)
            : base(name, scale, rotation, translation, model, texture)
        {
        }

        public override void Initialize()
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
           base.Draw(gameTime);
        }

    }//end class
}//end namespace