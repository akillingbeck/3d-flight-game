﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UsingVertices;

namespace GameLibrary
{
    public class BasicModel : Object3D
    {
        //Step 1 - add member variables
        protected Model model;

        protected Texture2D texture;
        //Step 3 - store transforms for bones
        Matrix[] transforms;

        //all models start at the origin before we apply world transform
        protected Vector3 position = Vector3.Zero;

        //always store the original position because we calculate
        //the current position based on transforming this point
        private Vector3 originalPosition = Vector3.Zero;

        //clean-dirty flag used to mark if world matrix update is necessary
        private bool bUpdateWorld = false;

        private Vector3 scale, rotation, translation;

        #region PROPERTIES


        public Matrix[] Transforms
        {
            get
            {
                return transforms;
            }
        }
        public Model Model
        {
            get
            {
                return model;
            }
        }
        public Vector3 Position
        {
            get { return position; }
        }
        public Vector3 Scale
        {
            get { return scale; }

            set
            {
                scale = value;
                bUpdateWorld = true;
            }
        }
        public Vector3 Rotation
        {
            get { return rotation; }

            set
            {
                rotation = value;
                bUpdateWorld = true;
            }
        }
        public Vector3 Translation
        {
            get { return translation; }

            set
            {
                translation = value;
                bUpdateWorld = true;
            }

        }
        #endregion

    
        //Step 2 - amend constructor for new parameters
        public BasicModel(string name, Vector3 scale, Vector3 rotation, Vector3 translation, Model model, Texture2D texture)
            : base(name, Vector3.Zero,Vector3.One,Vector3.One,Vector3.One)
        {
            this.scale = scale;
            this.rotation = rotation;

            this.rotation.X = MathHelper.ToRadians(rotation.X);
            this.rotation.Y = MathHelper.ToRadians(rotation.Y);
            this.rotation.Z = MathHelper.ToRadians(rotation.Z);

            this.translation = translation;

            this.world = Matrix.Identity
                * Matrix.CreateScale(scale)
                * Matrix.CreateRotationX(rotation.X)
                * Matrix.CreateRotationY(rotation.Y)
                * Matrix.CreateRotationZ(rotation.Z)
                * Matrix.CreateTranslation(translation);

            this.position = Vector3.Transform(originalPosition, world);

            this.model = model;
            this.texture = texture;

            //Step 3 - load bone transforms
            //Set transfroms
            this.transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
        }

        public override void Initialize()
        {
        }

        public override void Update(GameTime gameTime)
        {
            if (bUpdateWorld)
            {
                //every model must call this update so that world and position and set correctly
                this.world = Matrix.Identity
                   * Matrix.CreateScale(scale)
                   * Matrix.CreateRotationX(rotation.X)
                   * Matrix.CreateRotationY(rotation.Y)
                   * Matrix.CreateRotationZ(rotation.Z)
                   * Matrix.CreateTranslation(translation);

                //always transform the original position with the world matrix
                this.position = Vector3.Transform(originalPosition, world);

                bUpdateWorld = false;
            }
        }
        public override void Draw(GameTime gameTime)
        {

            //Loop through meshes and their effects 
            foreach (ModelMesh mesh in model.Meshes)
            {
                
                foreach (BasicEffect be in mesh.Effects)
                {
                    be.TextureEnabled = true;
                    be.Texture = texture;
                    be.Projection = game.cameraManager.ActiveCamera.Projection;
                    be.View = game.cameraManager.ActiveCamera.View;
                    be.World = world;
                }
                //Draw
                mesh.Draw();
            }
        }//end draw

    }//end class
}//end namespace