﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class RepairShop : TexturedCube
    {
        public float soundFxtime = 0;
        public BoundingBox boundingBox;
        public BillBoard board;
        public RepairShop(Vector3 Position)
            : base("Pickup", Vector3.One * 40f, Position, MiscAssets.buildingType1)
        {
            min = TexturedCubeParams.minCorner;
            max = TexturedCubeParams.maxCorner;
            boundingBox = new BoundingBox(min, max);
            board = new BillBoard(this, MiscAssets.repairShop, new Vector3(0, 80f, 0), new Vector3(50, 50, 50));
        }
        public override void Update(GameTime gameTime)
        {
            world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Position);
            boundingBox = new BoundingBox((min * scale) + Translation, (max * scale) + Translation);


            if (this.boundingBox.Intersects(game.plane.boundingSphere))
            {

                soundFxtime += gameTime.ElapsedGameTime.Milliseconds;
                if (soundFxtime >= 600f)
                {
                    MiscAssets.repair.Play();
                    soundFxtime = 0;
                }
              
                game.plane.health +=10;
                game.plane.armour += 10;
                game.plane.ammo +=10;

                if (game.plane.health > 200)
                {
                    game.plane.health = 200;
                }
                if (game.plane.armour > 200)
                {
                    game.plane.armour = 200;
                }
                if (game.plane.ammo > 500)
                {
                    game.plane.ammo = 500;
                }
            }

            base.Update(gameTime);
        }


    }
}
