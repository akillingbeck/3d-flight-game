using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
    public class TexturedCubeParams
    {
        public static VertexPositionColorTexture[] verts;
        public static VertexBuffer vertsBuffer;
        protected static short[] vertsIndices;
        public static IndexBuffer indexBuffer;
        public static VertexDeclaration vertexDeclaration;

        public static Vector3 minCorner = new Vector3(-1, -1, -1);
        public static Vector3 maxCorner = new Vector3(1, 1, 1);


        public static void Initialise(GraphicsDevice graphics, Vector2 tile)
        {

            
            verts = new VertexPositionColorTexture[24];
            //bottom face
            //bottom face
            verts[0] = new VertexPositionColorTexture(new Vector3(1, -1, -1), Color.White, new Vector2(0.5f, 0.5f));
            verts[1] = new VertexPositionColorTexture(new Vector3(-1, -1, -1), Color.White, new Vector2(0.25f, 0.5f));
            verts[2] = new VertexPositionColorTexture(new Vector3(-1, -1, 1), Color.White, new Vector2(0.25f, 0.75f));
            verts[3] = new VertexPositionColorTexture(new Vector3(1, -1, 1), Color.White, new Vector2(0.5f, 0.75f));

            //top face
            verts[4] = new VertexPositionColorTexture(new Vector3(-1, 1, -1), Color.White, new Vector2(0.25f, 0));
            verts[5] = new VertexPositionColorTexture(new Vector3(1, 1, -1), Color.White, new Vector2(0.5f, 0));
            verts[6] = new VertexPositionColorTexture(new Vector3(1, 1, 1), Color.White, new Vector2(0.5f, 0.25f));
            verts[7] = new VertexPositionColorTexture(new Vector3(-1, 1, 1), Color.White, new Vector2(0.25f, 0.25f));
            //back face
            verts[8] = new VertexPositionColorTexture(new Vector3(1, 1, -1), Color.White, new Vector2(0.5f, 0.25f));
            verts[9] = new VertexPositionColorTexture(new Vector3(-1, 1, -1), Color.White, new Vector2(0.25f, 0.25f));
            verts[10] = new VertexPositionColorTexture(new Vector3(-1, -1, -1), Color.White, new Vector2(0.25f, 0.5f));
            verts[11] = new VertexPositionColorTexture(new Vector3(1, -1, -1), Color.White, new Vector2(0.5f, 0.5f));
            //front face
            verts[12] = new VertexPositionColorTexture(new Vector3(-1, 1, 1), Color.White, new Vector2(0.75f, 0.25f));
            verts[13] = new VertexPositionColorTexture(new Vector3(1, 1, 1), Color.White, new Vector2(1, 0.25f));
            verts[14] = new VertexPositionColorTexture(new Vector3(1, -1, 1), Color.White, new Vector2(1, 0.5f));
            verts[15] = new VertexPositionColorTexture(new Vector3(-1, -1, 1), Color.White, new Vector2(0.75f, 0.5f));
            //left face
            verts[16] = new VertexPositionColorTexture(new Vector3(1, 1, 1), Color.White, new Vector2(0.25f, 0.25f));
            verts[17] = new VertexPositionColorTexture(new Vector3(1, 1, -1), Color.White, new Vector2(0, 0.25f));
            verts[18] = new VertexPositionColorTexture(new Vector3(1, -1, -1), Color.White, new Vector2(0, .5f));
            verts[19] = new VertexPositionColorTexture(new Vector3(1, -1, 1), Color.White, new Vector2(0.25f, 0.5f));
            //right face
            verts[20] = new VertexPositionColorTexture(new Vector3(-1, 1, -1), Color.White, new Vector2(0.5f, 0.25f));
            verts[21] = new VertexPositionColorTexture(new Vector3(-1, 1, 1), Color.White, new Vector2(0.75f, 0.25f));
            verts[22] = new VertexPositionColorTexture(new Vector3(-1, -1, 1), Color.White, new Vector2(0.75f, 0.5f));
            verts[23] = new VertexPositionColorTexture(new Vector3(-1, -1, -1), Color.White, new Vector2(0.5f, 0.5f));

            vertsIndices = new short[36];

            //bottom face
            vertsIndices[0] = 0;
            vertsIndices[1] = 1;
            vertsIndices[2] = 2;
            vertsIndices[3] = 0;
            vertsIndices[4] = 2;
            vertsIndices[5] = 3;
            
            vertsIndices[6] = 4;
            vertsIndices[7] = 5;
            vertsIndices[8] = 6;
            vertsIndices[9] = 4;
            vertsIndices[10] = 6;
            vertsIndices[11] = 7;
            
            vertsIndices[12] = 8;
            vertsIndices[13] = 9;
            vertsIndices[14] = 10;
            vertsIndices[15] = 8;
            vertsIndices[16] = 10;
            vertsIndices[17] = 11;

            vertsIndices[18] = 12;
            vertsIndices[19] = 13;
            vertsIndices[20] = 14;
            vertsIndices[21] = 12;
            vertsIndices[22] = 14;
            vertsIndices[23] = 15;

            vertsIndices[24] = 16;
            vertsIndices[25] = 17;
            vertsIndices[26] = 18;
            vertsIndices[27] = 16;
            vertsIndices[28] = 18;
            vertsIndices[29] = 19;

            vertsIndices[30] = 20;
            vertsIndices[31] = 21;
            vertsIndices[32] = 22;
            vertsIndices[33] = 20;
            vertsIndices[34] = 22;
            vertsIndices[35] = 23;

            //reserve space on VRAM of GFX for verts buffer
            vertsBuffer = new VertexBuffer(graphics,
                typeof(VertexPositionColorTexture), verts.Length, BufferUsage.WriteOnly);
            //reserve space on VRAM of GFX for indices
            indexBuffer = new IndexBuffer(graphics,
                typeof(short), vertsIndices.Length, BufferUsage.WriteOnly);

            //load the vertex information on GFX
            vertsBuffer.SetData(verts, 0, verts.Length); //may use subset of contents
            //load the index information on GFX
            indexBuffer.SetData(vertsIndices); //always use all contents
         }
    }
}