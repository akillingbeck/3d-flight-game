using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using UsingVertices;

namespace GameLibrary
{
   //this version of the TexturedCube is based on a single quad that is transformed
   //in 3D space using 6 world transforms - the result is a cube - with limitations!
    public class TexturedCube : Object3D
    {
        protected Texture2D texture;
        //separate the transforms!
        Vector3 Vscale;
     

        //will be used to test for contains() against Camera frustum
   //     protected BoundingBox boundingBox;


        public TexturedCube(string name, Vector3 scale, Vector3 Position, Texture2D texture)
            : base(name, Position, scale, TexturedCubeParams.minCorner, TexturedCubeParams.maxCorner)
        {
            this.texture = texture;
            this.Vscale = scale;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
           // rotation *= Matrix.CreateRotationY(MathHelper.ToRadians(3));
          //  translation += new Vector3(0.1f, 0, 0);
            //if we never intend to change the position of the object using Update()
            //then we can place the world calculation here and do it once
          //  this.world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(translation);
            world = Matrix.Identity * Matrix.CreateScale(Vscale) * rotation * Matrix.CreateTranslation(Position);
            this.boundingSphere = new BoundingSphere((centre) + Translation, radius);
          //  DebugShapeRenderer.AddBoundingSphere(boundingSphere,Color.Green);


            base.Update(gameTime);
        }
        public override void onCollide(Object3D obj)
        {

            base.onCollide(obj);
        }
        /// <summary>
        /// Allows the game component to draw.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            //if box inside (or contained) by frustum then draw it
            if ((game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Contains)
                || (game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Intersects))
           {
                game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                game.textureEffect.World = world;
                game.textureEffect.Texture = texture;

                game.textureEffect.CurrentTechnique.Passes[0].Apply();
                game.GraphicsDevice.SetVertexBuffer(TexturedCubeParams.vertsBuffer);
                game.GraphicsDevice.Indices = TexturedCubeParams.indexBuffer;
                game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 24, 0, 12);
             //   DebugShapeRenderer.Draw(gameTime, game.cameraManager.ActiveCamera.View, game.cameraManager.ActiveCamera.Projection, world);
            }

               
                base.Draw(gameTime);
        }
    }
}