﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class ArmyBase
    {

        public List<ArmyBarracks> baseBuildings = new List<ArmyBarracks>();
        public Vector3 position;
        public Building tower;
        public Building tower2;

        public ArmyBase(Vector3 Position,int rows,int columnns)
        {
            this.position = Position;
            tower = new Building(Position + new Vector3(40,0,35), MiscAssets.buildingType2, new Vector3(40,80,40));
            tower2 = new Building(Position + new Vector3(140, 0, 135), MiscAssets.buildingType2, new Vector3(30, 60, 20));
            Random rand = new Random();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columnns; j++)
                {
                    int scale = rand.Next(10,40);
                    ArmyBarracks bar = new ArmyBarracks(Position + new Vector3(i*scale*3f,0,j*scale*3f), MiscAssets.army, scale);
                }

            }
        }
    }
}
