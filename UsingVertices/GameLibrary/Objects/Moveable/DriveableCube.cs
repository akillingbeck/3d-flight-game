﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UsingVertices;
namespace GameLibrary
{
    public class DriveableCube : MoveableCube
    {
        public DriveableCube(string name, Vector3 scale, Matrix rot, 
                        Vector3 translation, Texture2D texture,
                        Vector3 look, Vector3 up)
            : base(name, scale, rot, translation, texture, look, up)
        {
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}