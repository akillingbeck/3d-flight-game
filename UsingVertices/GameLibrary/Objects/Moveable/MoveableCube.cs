﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UsingVertices;
namespace GameLibrary
{
    public class MoveableCube : TexturedCube
    {
        protected Vector3 look; //direction of movement
        protected Vector3 up; //vector pointing upwards through box
        protected Vector3 right; //vector pointing in right direction (for strafing)
        protected float moveSpeed = 0.05f, strafeSpeed = 0.01f;
        protected float rotationSpeed = MathHelper.ToRadians(0.05f);

        public MoveableCube(string name, Vector3 scale, Matrix rot, 
                        Vector3 position, Texture2D texture,
                        Vector3 look, Vector3 up)
            : base(name, scale, position, texture)
        {
            this.look = Vector3.Normalize(look); 
            this.up = Vector3.Normalize(up);
            this.right = Vector3.Normalize(Vector3.Cross(this.look, this.up));
        }

        //override the Object3D implementations of these methods to provide the correct vector values to thirdpersoncamera
        public override Vector3 getLook()
        {
            return look; 
        }
        public override Vector3 getUp()
        {
            return up; 
        }
        public override Vector3 getRight()
        {
            return right;  
        }

        public override void Initialize()
        {  
            base.Initialize();
        }

        private void handleInput(GameTime gameTime)
        {
            float rotationIncr = gameTime.ElapsedGameTime.Milliseconds * rotationSpeed;
         
            if (game.keyboardManager.isKeyDown(Keys.I))
            {
               this.POSITION += look * gameTime.ElapsedGameTime.Milliseconds * moveSpeed;
            }
            else if (game.keyboardManager.isKeyDown(Keys.K))
            {
                this.POSITION -= look * gameTime.ElapsedGameTime.Milliseconds * moveSpeed;
            }
            if (game.keyboardManager.isKeyDown(Keys.J))
            {
                Matrix rotMatrix = Matrix.CreateRotationY(rotationIncr);
                //this line modifies the visual of the cube on the screen
                this.rotation *= rotMatrix;
                //these lines modify the look and right vectors - the drive vectors
                this.look = Vector3.Transform(look, rotMatrix);
                look.Normalize();
                this.right = Vector3.Cross(look, up);
                right.Normalize();
            }
            else if (game.keyboardManager.isKeyDown(Keys.L))
            {
                Matrix rotMatrix = Matrix.CreateRotationY(-rotationIncr);
                //this line modifies the visual of the cube on the screen
                this.rotation *= rotMatrix;
                //these lines modify the look and right vectors - the drive vectors
                this.look = Vector3.Transform(look, rotMatrix);
                look.Normalize();
                this.right = Vector3.Cross(look, up);
                right.Normalize();
            }
        }
        public override void Update(GameTime gameTime)
        {
            handleInput(gameTime);
            base.Update(gameTime); //call update of base to set world matrix
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime); //draw cube on screen!
        }
    }
}


/*
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UsingVertices;

namespace GameLibrary
{
    public class MoveableCube : TexturedCube
    {
        public MoveableCube()
            : base()
        {
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        public override void Update(GameTime gameTime)
        {
        }
        public override void Draw(GameTime gameTime)
        {
        
        }
    }
}
  */