﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameLibrary
{

     
    public class PlaneParams
    {

        public static VertexPositionColorTexture[] verts;
        public static VertexBuffer vertsBuffer;
        public static IndexBuffer indexBuffer;
        protected static short[] vertsIndices;

        public static void initialise(GraphicsDevice graphics)
        {
            verts = new VertexPositionColorTexture[24];

            //left wing-tip vert
            verts[0] = new VertexPositionColorTexture(new Vector3(-2, 2, 0), Color.White, new Vector2(0, 0.4375f));

            //left wing-back vert
            verts[1] = new VertexPositionColorTexture(new Vector3(-2, 0, 1), Color.White, new Vector2(0, 1f));

            //left wing-front vert
            verts[2] = new VertexPositionColorTexture(new Vector3(-2, 0, -1), Color.White, new Vector2(0.1875f, 0.6875f));




            //hull left
           verts[3] = new VertexPositionColorTexture(new Vector3(-2, 0, 1), Color.White, new Vector2(0, 1));
            verts[4] = new VertexPositionColorTexture(new Vector3(0, 0, -4), Color.White, new Vector2(0.5f, 0));
            verts[5] = new VertexPositionColorTexture(new Vector3(0, 2, 1), Color.White, new Vector2(0.5f, 0.6875f));
            
              //hull Right
              verts[6] = new VertexPositionColorTexture(new Vector3(0, 0, -4), Color.White, new Vector2(0.5f, 0));
              verts[7] = new VertexPositionColorTexture(new Vector3(0, 2, 1), Color.White, new Vector2(0.5f, 0.6875f));
              verts[8] = new VertexPositionColorTexture(new Vector3(2, 0, 1), Color.White, new Vector2(1, 1));


             
            //right wing-tip vert
            verts[9] = new VertexPositionColorTexture(new Vector3(2, 2, 0), Color.White, new Vector2(1, 0.4375f));

            //right wing-back vert
            verts[10] = new VertexPositionColorTexture(new Vector3(2, 0, 1), Color.White, new Vector2(1, 1));

            //right wing-front vert
            verts[11] = new VertexPositionColorTexture(new Vector3(2, 0, -1), Color.White, new Vector2(0.8125f, 0.6875f));


            //left hull - wing connection
            verts[12] = new VertexPositionColorTexture(new Vector3(-2, 0, 1), Color.White, new Vector2(0, 1));
            verts[14] = new VertexPositionColorTexture(new Vector3(0, 0, -4), Color.White, new Vector2(0,0.4375f));
            verts[13] = new VertexPositionColorTexture(new Vector3(-2, 0, -1), Color.White, new Vector2(0.1875f, 0.6875f));

            //right hull - wing connection
            verts[15] = new VertexPositionColorTexture(new Vector3(2, 0, 1), Color.White, new Vector2(0, 1));
            verts[16] = new VertexPositionColorTexture(new Vector3(0, 0, -4), Color.White, new Vector2(0, 0.4375f));
            verts[17] = new VertexPositionColorTexture(new Vector3(2, 0, -1), Color.White, new Vector2(0.1875f, 0.6875f));

            //back hull 
            verts[18] = new VertexPositionColorTexture(new Vector3(-2, 0, 1), Color.White, new Vector2(0, 1));
            verts[19] = new VertexPositionColorTexture(new Vector3(0, 2, 1), Color.White, new Vector2(0.5f, 0.6875f));
            verts[20] = new VertexPositionColorTexture(new Vector3(2, 0, 1), Color.White, new Vector2(1, 1));


            //bottom
            verts[21] = new VertexPositionColorTexture(new Vector3(0, 0, -4), Color.White, new Vector2(0, 0));
            verts[22] = new VertexPositionColorTexture(new Vector3(-2, 0, 1), Color.White, new Vector2(1, 0));
            verts[23] = new VertexPositionColorTexture(new Vector3(2, 0, 1), Color.White, new Vector2(0.5f,0.25f));

        
           
            //verts[12] = new VertexPositionColorTexture(new Vector3(0, -1, 1), Color.White, new Vector2(1, 1));
            /* 
           vertsIndices = new short[16];

                           vertsIndices[0] = 1;
                           vertsIndices[1] = 0;
                           vertsIndices[2] = 2;
                           vertsIndices[3] = 1;

                           vertsIndices[4] = 3;
                           vertsIndices[5] = 5;
                           vertsIndices[6] = 4;
                           vertsIndices[7] = 3;

                           vertsIndices[8] = 11;
                           vertsIndices[9] = 10;
                           vertsIndices[10] = 9;
                           vertsIndices[11] = 11;

                           vertsIndices[12] = 8;
                           vertsIndices[13] = 7;
                           vertsIndices[14] = 6;
                           vertsIndices[15] = 8;
                           */

          
            vertsBuffer = new VertexBuffer(graphics, typeof(VertexPositionColorTexture), verts.Length, BufferUsage.WriteOnly);

            //indexBuffer = new IndexBuffer(graphics, typeof(short), vertsIndices.Length, BufferUsage.WriteOnly);


            vertsBuffer.SetData(verts, 0, verts.Length);
           // indexBuffer.SetData(vertsIndices);

        }

    }
}
