﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameLibrary
{
    public class Missile : Object3D
    {

        public  enum OWNER { Enemy=0, Player }public OWNER owner;
        public static Texture2D texture;

        public float TTL = 0;
        public int secondsPast = 0;
        private Vector3 Direction;
        public static VertexPositionColorTexture[] verts;
        public static VertexBuffer vertsBuffer;
        public static void initialise(GraphicsDevice graphics)
        {
            verts = new VertexPositionColorTexture[6];
            verts[0] = new VertexPositionColorTexture(new Vector3(-1, 0, 0), Color.White, new Vector2(0, 0));
            verts[1] = new VertexPositionColorTexture(new Vector3(0,1, 0), Color.White, new Vector2(0,1));
            verts[2] = new VertexPositionColorTexture(new Vector3(1, 0, 0), Color.White, new Vector2(1, 1));

            verts[3] = new VertexPositionColorTexture(new Vector3(0, 0, -1), Color.White, new Vector2(0, 0));
            verts[4] = new VertexPositionColorTexture(new Vector3(-1, 0, 0), Color.White, new Vector2(0, 0));

            verts[5] = new VertexPositionColorTexture(new Vector3(0, 1, 0), Color.White, new Vector2(0, 0));

            vertsBuffer = new VertexBuffer(graphics,
               typeof(VertexPositionColorTexture), verts.Length, BufferUsage.WriteOnly);


            vertsBuffer.SetData(verts,0,verts.Length);
        }

        public Missile(Vector3 direction,Vector3 Position,OWNER owner):base("Photon",Position,Vector3.One * 0.3f,new Vector3(-1,0,0),new Vector3(1,1,-1))
        {
            this.owner = owner;
            this.Direction = direction;
            this.Translation = Position + new Vector3(0,1,0);

        }
        public override void Update(GameTime gameTime)
        {
            this.Translation += Direction * 160f * game.delta;
            TTL += gameTime.ElapsedGameTime.Milliseconds;

            if (TTL >= 3000f)
            {
                game.objectManager.Remove(this);
            }

            world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Translation);
            this.boundingSphere = new BoundingSphere((centre) + Translation, centre.X + max.X);


            for (int i = 0; i < game.objectManager.Count; i++)
            {
                if (game.objectManager[i].bCollideable)
                {
                    if (game.objectManager[i].boundingSphere.Intersects(this.boundingSphere)
                        || this.boundingSphere.Intersects(game.objectManager[i].boundingSphere))
                    {
                        game.objectManager.Remove(this);
                        game.objectManager[i].onCollide(this);
                        MiscAssets.hit.Play();
                    }
                }
            }

            if (null != game.objectiveManager.currentObjective.objectives )
            {
                for (int i = 0; i < game.objectiveManager.currentObjective.objectives.Count; i++)
                {
                    if (this.owner != OWNER.Enemy)
                    {
                        if (game.objectiveManager.currentObjective.objectives[i].boundingSphere.Intersects(this.boundingSphere)
                              || this.boundingSphere.Intersects(game.objectiveManager.currentObjective.objectives[i].boundingSphere))
                        {
                            MiscAssets.hit.Play();
                            game.objectManager.Remove(this);
                            game.objectiveManager.currentObjective.objectives[i].onCollide(this);
                            if (game.objectiveManager.currentObjective.objectives == null)
                            {

                                break;
                            }
                        }
                    }

                }
            }
            else if (game.objectiveManager.currentObjective.objective != null)
            {
                if (this.owner != OWNER.Enemy)
                {
                    if (this.boundingSphere.Intersects(game.objectiveManager.currentObjective.objective.boundingSphere))
                    {
                        MiscAssets.hit.Play();
                        game.objectiveManager.currentObjective.objective.onCollide(this);
                    }
                }
            }

            if (this.boundingSphere.Intersects(game.plane.boundingSphere))
            {
                if (this.owner != OWNER.Player)
                {
                    MiscAssets.hit.Play();
                    game.objectManager.Remove(this);
                    game.plane.onCollide(this);
                }
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if ((game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Contains)
              || (game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Intersects))
            {
                game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                game.textureEffect.World = world;
                game.textureEffect.Texture = texture;

                game.textureEffect.CurrentTechnique.Passes[0].Apply();
                game.GraphicsDevice.SetVertexBuffer(vertsBuffer);
                //  game.GraphicsDevice.Indices = PlaneParams.indexBuffer;
                game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>
                    (PrimitiveType.TriangleStrip, verts, 0, 4);

            }

           // DebugShapeRenderer.Draw(gameTime, game.cameraManager.ActiveCamera.View, game.cameraManager.ActiveCamera.Projection, world);
            base.Draw(gameTime);
        }

    }
}
