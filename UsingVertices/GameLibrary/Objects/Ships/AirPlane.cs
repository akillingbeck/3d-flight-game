﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UsingVertices;

namespace GameLibrary
{
    public class AirPlane : Object3D
    {
        public bool isLanding = false;
        public static bool gotHome = false;
        public Texture2D pixelTexture;
        public Rectangle hudBg;
        public static Texture2D crosshair;
        private float maxThrust = 5000f;
        private const float levelLimit = 4000f;
        private const float seaLevel = 0.0f;
        private const float upperAtmosphere = 2000f;
        private const float airdrag = 0.97f;
        private const float RotationSpeed = 1.2f;
        private const float RollSpeed = 4.2f;
        private const float mass = 50.0f;
        private bool stopMouse = false;


        private float humLoop = 10000f;
        public float fuel = 200f;
        public float waitForRefill = 0;
        protected bool needFuel = false;
        public int speed;
        public int accelLength = 0;

        private bool reloading = false;
        private float reloadPassed;

        private bool rollingLeft = false, rollingRight = false;

        public float health=100, armour=100, ammo = 500;
        public float fireRate = 60f;
        protected float fireTime = 0f;

        public int objectivesCompleted = 0;
        public int objectivesFailed = 0;
        protected float rollAngle = 0;
        public Vector3 Direction;
        public Vector3 oldPosition;
        public Vector3 Up;

        public static int enemiesKilled = 0;
        private Vector3 right;
        public Vector3 Right
        {
            get { return right; }
        }
        bool dead = false;
        public Vector3 velocity;
        Vector2 rotationAmount;
        protected Texture2D texture;

      
        float rollX;

   
        public AirPlane(Texture2D texture,Vector3 scale,Vector3 Position)
            : base("Player", Position,scale, new Vector3(-2, 0, 1) , new Vector3(2, 2, -4) )
        {


            pixelTexture = new Texture2D(game.GraphicsDevice, 1, 1);
            pixelTexture.SetData(new Color[]{Color.White});
        

            this.scale = scale;
          
          
            this.texture = texture;
            this.rotation = Matrix.Identity;

            Direction = Vector3.Forward;
            Up = Vector3.Up;
            right = Vector3.Right;
            Reset();
          //  MiscAssets.hum.Play();

        }
        public void Reset()
        {
            if (dead)
            {
                dead = false;
                Position = new Vector3(0f, 10f, 0f);
            }


            velocity = Vector3.Zero;
            health = 100;
            armour = 100;
            fuel = 200f;
          
        }
        public void reduceHealthArmour(float scalar = 1f)
        {
          

            if (this.armour > 0)
            {
                this.health -= (5f * 0.2f) * scalar;
                this.armour -= 3f * scalar;
            }
            else
            {
                this.health -= 2f;
            }

            if (health < 0)
            {
                dead = true;
                Reset();
            }
        }
        public void updateInputFromMouse(GameTime gameTime)
        {
            if (!stopMouse)
            {
                // rotationAmount.X
                rollX += (game.mouseManager.NEWSTATE.X - game.viewportCentre.X) * MathHelper.ToRadians(90) * game.delta;
                rotationAmount.Y += (game.mouseManager.NEWSTATE.Y - game.viewportCentre.Y) * MathHelper.ToRadians(90) * game.delta;
                Mouse.SetPosition(game.viewportCentre.X, game.viewportCentre.Y);

                if (game.mouseManager.isLeftButtonPressed())
                {
                    if (ammo > 0)
                    {
                        fireTime += gameTime.ElapsedGameTime.Milliseconds;
                        if (fireTime >= fireRate)
                        {
                            Missile m = new Missile(Direction, Position,Missile.OWNER.Player);
                            fireTime = 0;
                            ammo--;
                            MiscAssets.shoot.Play();
                        }
                    }
                    else
                    {
                        reloading = true;

                    }
                }

                if (reloading)
                {
                    reloadPassed += gameTime.ElapsedGameTime.Milliseconds;
                    if (reloadPassed >= 2000f)
                    {
                        reloading = false;
                        reloadPassed = 0;
                        ammo = 50;
                    }

                }
            }
            else
            {

            }
        }
        private void updateInputFromKeyboard(GameTime gameTime)
        {
            if (maxThrust > 5000f)
            {
                maxThrust -= 4f;
            }
            if (game.keyboardManager.isKeyDown(Keys.A))
                rotationAmount.X = 1.0f;
            if (game.keyboardManager.isKeyDown(Keys.D))
                rotationAmount.X = -1.0f;
            if (game.keyboardManager.isKeyDown(Keys.Up))
                rotationAmount.Y = -1.0f;
            if (game.keyboardManager.isKeyDown(Keys.Down))
                rotationAmount.Y = 1.0f;

            if (game.keyboardManager.isKeyPressed(Keys.P))
            {
                stopMouse = !stopMouse;
            }

            if (game.keyboardManager.isKeyDown(Keys.LeftShift))
            {
                if (fuel > 0)
                {
                    maxThrust = 14000f;
                    fuel -= 0.5f;
                }
                else
                {
                    needFuel = true;
                }
            }
            if (needFuel)
            {
                waitForRefill += gameTime.ElapsedGameTime.Milliseconds;
                if (waitForRefill >= 10000)
                {
                    fuel = 200f;
                    waitForRefill = 0f;
                    needFuel = false;
                }
            }
        }
        public override void Update(GameTime gameTime)
        {

           
                bCollision = false;

                rotationAmount = Vector2.Zero;
                rollX = 0f;


                updateInputFromKeyboard(gameTime);
                updateInputFromMouse(gameTime);




                rotationAmount = rotationAmount * RotationSpeed * game.delta;
                rollX = rollX * RollSpeed * game.delta;

                if (Up.Y < 0)
                    rotationAmount.X = -rotationAmount.X;

                updateWorld();
                //rotation for yaw and/or pitch


                float amountThrust = 0f;
                if (game.keyboardManager.isKeyDown(Keys.W))
                    amountThrust = 1.0f;
                if (game.keyboardManager.isKeyDown(Keys.S))
                    amountThrust = -1.0f;

                updatePosition(amountThrust);



                world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Position);
                this.boundingSphere = new BoundingSphere(((centre) + Translation), radius);


                System.Diagnostics.Debug.WriteLine("clear");
                checkCollisions();

                if (!bCollision)
                    oldPosition = Position;
         
            base.Update(gameTime);
        }
        public void updateWorld()
        {
            Matrix rotationMatrix = Matrix.CreateFromAxisAngle(Direction, rollX) *
          Matrix.CreateFromAxisAngle(Right, rotationAmount.Y) *
          Matrix.CreateRotationY(rotationAmount.X);

            this.rotation *= rotationMatrix;
            // Matrix rollMatrix = ;
         


            // Rotate vectors for orientation
            Direction = Vector3.TransformNormal(Direction, rotationMatrix);
            Up = Vector3.TransformNormal(Up, rotationMatrix);


            Direction.Normalize();
            Up.Normalize();

            right = Vector3.Cross(Direction, Up);

            Up = Vector3.Cross(Right, Direction);
            Up.Normalize();
        }
        public void checkCollisions()
        {
            for (int i = 0; i < game.objectManager.Count; i++)
            {
                if (game.objectManager[i].bCollideable)
                {
                    if (game.objectManager[i].boundingSphere.Intersects(this.boundingSphere)
                        || this.boundingSphere.Intersects(game.objectManager[i].boundingSphere))
                    {
                        System.Diagnostics.Debug.WriteLine("Collision");
                        game.objectManager[i].onCollide(this);
                        bCollision = true;
                    }

                }
            }

            //if (this.boundingSphere.Intersects(game.objectiveManager.currentObjective.objective.boundingSphere) &&
            //    game.objectiveManager.currentObjective.objective.bCollideable)
            //{
            //    game.objectiveManager.currentObjective.objective.onCollide(this);
            //}
        }
        public void updatePosition(float amountThrust)
        {
            Vector3 forceApplied = Direction * amountThrust * maxThrust;

            
            Vector3 acceleration = forceApplied / mass;

            velocity += acceleration * game.delta;
            velocity *= airdrag;

            speed = (int)velocity.Length();
            if (speed == 0)
            {
              
            }
            //velocity += new Vector3(0, -9.81f * game.delta, 0);

            accelLength = (int)acceleration.Length();

            checkLevelLimits();
            Position += velocity * game.delta;
        }
        private void checkLevelLimits()
        {
            if (Position.Y <= seaLevel)
            {
                velocity.Y = -velocity.Y;
                reduceHealthArmour(5);
            }
            else if (Position.Y >= upperAtmosphere)
            {
                velocity.Y = -velocity.Y;
                
            }

            if (Position.X <= -levelLimit)
            {
                velocity.X = -velocity.X;
             
            }
            else if (Position.X >= levelLimit)
            {
                velocity.X = -velocity.X;
               
            }
            if (Position.Z <= -levelLimit)
            {
                velocity.Z = -velocity.Z;
                
            }
            else if (Position.Z >= levelLimit)
            {
                velocity.Z = -velocity.Z;
               
            }

        }
        public override void onCollide(Object3D obj)
        {
            reduceHealthArmour();
            base.onCollide(obj);
        }
        public override void Draw(GameTime gameTime)
        {
            //if box inside (or contained) by frustum then draw it
            if ((game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Contains)
              || (game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Intersects))
            {
                //  game.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
              //  game.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                //  game.textureEffect.Alpha = 1f;
                game.textureEffect.World = world;
                game.textureEffect.Texture = texture;

                game.textureEffect.CurrentTechnique.Passes[0].Apply();
                game.GraphicsDevice.SetVertexBuffer(PlaneParams.vertsBuffer);
                //  game.GraphicsDevice.Indices = PlaneParams.indexBuffer;
                game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>
                    (PrimitiveType.TriangleList, PlaneParams.verts, 0, 8);
            }
              //  DebugShapeRenderer.Draw(gameTime, game.cameraManager.ActiveCamera.View, game.cameraManager.ActiveCamera.Projection, world);
               
            String healthText  = "Health "+(int)health;
            String armourText = "Armour " + (int)armour;
            String ammoText = "Ammo " + ammo;
            String speedText = "Speed " + speed+" m/s";
            String accelText = "Acceleration " + accelLength + " m/s/s";
            String fuelText = "AfterBurner " + (int)fuel ;
            Vector2 fuelSize = game.font.MeasureString(fuelText);
            Vector2 speedSize = game.font.MeasureString(speedText);
            Vector2 accelSize = game.font.MeasureString(accelText);
            Vector2 healthSize = game.font.MeasureString(healthText);

           game.spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, DepthStencilState.Default,
                RasterizerState.CullNone);
               // game.spriteBatch.Draw(MiscAssets.hudBg,new Vector2(MiscAssets.hudBg.Width,game.viewportCentre.Y),Color.White);

            
               
                game.spriteBatch.DrawString(game.font, healthText, new Vector2(0, game.viewportCentre.Y), Color.White,0f,Vector2.Zero,1f,SpriteEffects.None,0f);
                game.spriteBatch.DrawString(game.font, armourText, new Vector2(0, game.viewportCentre.Y + healthSize.Y), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                game.spriteBatch.DrawString(game.font, ammoText, new Vector2(0, game.viewportCentre.Y + (healthSize.Y * 2)), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

              

              
                game.spriteBatch.DrawString(game.font, speedText, new Vector2(game.GraphicsDevice.Viewport.Width - speedSize.X, game.viewportCentre.Y), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                game.spriteBatch.DrawString(game.font, accelText, new Vector2(game.GraphicsDevice.Viewport.Width - accelSize.X, game.viewportCentre.Y + (accelSize.Y)), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                game.spriteBatch.DrawString(game.font, fuelText, new Vector2(game.GraphicsDevice.Viewport.Width - fuelSize.X, game.viewportCentre.Y + (accelSize.Y) * 2), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                game.spriteBatch.Draw(pixelTexture, hudBg, null, Color.Black, 0f, Vector2.Zero, SpriteEffects.None, 0.9f);
                if (!ObjectiveManager.allComplete)
                {
                    game.spriteBatch.DrawString(game.font, "Current Objective: " + game.objectiveManager.currentObjective.OBJECTIVE_NAME,
                        new Vector2(0, 0), Color.Green);

                  
                }
                else if (ObjectiveManager.allComplete)
                {
                    game.spriteBatch.DrawString(game.font, "Current Objective: Have a cup of tea, its over!",
                       new Vector2(0, 0), Color.White);
                }
                game.spriteBatch.DrawString(game.font, "Objectives Completed " + game.plane.objectivesCompleted,
                         new Vector2(0, 20), Color.Green);
                game.spriteBatch.DrawString(game.font, "Objectives Failed " + game.plane.objectivesFailed,
                 new Vector2(0, 40), Color.Red);

                if (reloading)
                {
                    String text ="RELOADING";
                    Vector2 textSize = game.font.MeasureString(text);
                    game.spriteBatch.DrawString(game.font, text, new Vector2(game.viewportCentre.X, game.viewportCentre.Y), Color.White,
                        0f,new Vector2(textSize.X/2,textSize.Y/2),3f,SpriteEffects.None,0f);
                }
                game.spriteBatch.End();
                game.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                base.Draw(gameTime);
            
        }


     
    }
}
