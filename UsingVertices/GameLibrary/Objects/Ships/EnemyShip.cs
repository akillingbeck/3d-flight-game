﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UsingVertices;

namespace GameLibrary
{
    public class EnemyShip : ObjectiveObject
    {

      
        private const float maxThrust = 7000f;
        private const float seaLevel = 200.0f;
        private const float airdrag = 0.97f;
        private const float RotationSpeed = 1.2f;
        private const float RollSpeed = 4.2f;
        private const float mass = 100.0f;


        public int health = 100;
        protected float rollAngle = 0;
        public Vector3 Direction;
        public Vector3 Up;

        private Vector3 right;
        public Vector3 Right
        {
            get { return right; }
        }

        public Vector3 velocity;


        public float fireRate = 400f;
        protected float fireTime = 0f;
        Vector2 rotationAmount;
        protected Texture2D texture;
        public Vector3 Position;

        Matrix turnMat;
       // protected Vector3 scale;

        float rollX;


        public EnemyShip(String name,Vector3 scale,Vector3 position)
            : base(name, position, scale, new Vector3(-2, 0, 1), new Vector3(2, 2, -4),MiscAssets.enemyShip)
        {

            this.Position = position;
            this.texture = MiscAssets.enemyShip;
            bCollideable = true;
            turnMat = Matrix.Identity;
            min = new Vector3(-2, 0, 1);
            max = new Vector3(2, 2, -4);
            boundingBox = new BoundingBox(min * scale, max * scale);
            Reset();
        //    label = new BillBoard(this, ObjectiveArrowParams.texture, new Vector3(0, 5f, 0), scale);
          //  label.doBob = false;
           // this.boundingSphere = new BoundingSphere(centre + Position, (centre.X + max.X) * 40f);
        }
        public override void onCollide(Object3D obj)
        {
            if (obj is AirPlane)
            {
                AirPlane plane = (AirPlane)obj;
                plane.velocity = -plane.velocity;
                plane.health -= 1;
                System.Diagnostics.Debug.WriteLine("Colliding");
            }
            if (obj is Missile)
            {
                this.health -= 5;
                if (health < 0)
                {
                    AirPlane.enemiesKilled++;
                    if (game.objectiveManager.currentObjective.objectives != null)
                        game.objectiveManager.currentObjective.objectives.Remove(this);
                    else
                    {
                        game.objectiveManager.currentObjective.objective = null;
                        game.billBoardManager.Remove(this.label);
                        game.plane.objectivesCompleted++;
                        game.objectiveManager.nextObjective();
                    }
                    game.billBoardManager.Remove(this.label);
                  

                    if (AirPlane.enemiesKilled == game.objectiveManager.currentObjective.listSize)
                    {
                        game.plane.objectivesCompleted++;
                        game.objectiveManager.nextObjective();
                    }
                   // if (game.objectiveManager.currentObjective.objectives.Count == 0)
                   // {
                        //game.objectiveManager.remove(game.objectiveManager.currentObjective);
                   // }
                   // game.objectiveManager.nextObjective();
                    
                }
            }
            base.onCollide(obj);
        }
        public void Reset()
        {
           // Position = new Vector3(0, 0, 0);
            Direction = Vector3.Forward;
            Up = Vector3.Up;
            right = Vector3.Right;
            velocity = Vector3.Zero;

        }

        private void updatePosition()
        {
       
         
           //Direction = Vector3.Transform(Direction, turnMat);
           //Direction.Normalize();
           //Up = Vector3.Transform(Up, turnMat);
           //Up.Normalize();
           //right = Vector3.Cross(Up, Direction);
           //right.Normalize();

           //rotation *= turnMat;

           Vector3 forceApplied = Direction * 2.0f * maxThrust;


           Vector3 acceleration = forceApplied / mass;

           velocity += acceleration * game.delta;
           velocity *= airdrag;

           Position +=velocity * game.delta;

           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position">Position of object</param>
        /// <param name="desiredDirection">Position of object to face</param>
        /// <param name="U">Up vector</param>
        /// <returns></returns>
        protected Matrix RotateToFace(Vector3 position, Vector3 desiredDirection, Vector3 U)
        {
            Vector3 Dir = (position - desiredDirection);
            Vector3 Right = Vector3.Cross(U, Dir);
            Vector3.Normalize(ref Right, out Right);
            Vector3 Backwards = Vector3.Cross(Right, U);
            Vector3.Normalize(ref Backwards, out Backwards);
            Vector3 Up = Vector3.Cross(Backwards, Right);
            Matrix rot = new Matrix(Right.X, Right.Y, Right.Z, 0, Up.X, Up.Y, Up.Z, 0, Backwards.X, Backwards.Y, Backwards.Z, 0, 0, 0, 0, 1);
            return rot;
        }
        public void attack(GameTime gameTime)
        {
            fireTime += gameTime.ElapsedGameTime.Milliseconds;
            if (fireTime >= fireRate)
            {
                Missile m = new Missile(Direction, Position,Missile.OWNER.Enemy);
                fireTime = 0;
                
               
            }
        }
        public void updateDirection()
        {
            Direction = game.plane.POSITION - this.Position;
            Direction.Normalize();

            rotation = RotateToFace(Position, game.plane.POSITION, Up);
            
            
        }
        public override void Update(GameTime gameTime)
        {
            if (active)
            {
                rotationAmount = Vector2.Zero;
                float distanceFromPlayer = Vector3.Distance(Position, game.plane.POSITION);
                turnMat = Matrix.Identity;

                updateDirection();

                if (distanceFromPlayer < 500)
                {
                    attack(gameTime);
                }

                if (distanceFromPlayer > 300f)
                    updatePosition();
                else
                {
                    velocity = Vector3.Zero;
                }

              
                rayPicked = false;
                Ray mouseRay = game.cameraManager.ActiveCamera.GetPickRay(game.mouseManager.Position, game.GraphicsDevice.Viewport);
                if (mouseRay.Intersects(this.boundingSphere) != null)
                {
                    rayPicked = true;
                }
                //  Position += new Vector3(0, 0, -0.8f);
                
                world = Matrix.Identity * Matrix.CreateScale(scale) * rotation * Matrix.CreateTranslation(Position);
                this.boundingSphere = new BoundingSphere((centre + Translation) + new Vector3(0, -radius, 0), radius / 2f);
                boundingBox = new BoundingBox((min * scale) + Translation, (max * scale) + Translation);


                if (game.plane.boundingSphere.Intersects(this.boundingSphere))
                {
                    onCollide(game.plane);
                }
            }
            base.Update(gameTime);
        }
     
        public override void Draw(GameTime gameTime)
        {
            if (active)
            {
                if ((game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere)== ContainmentType.Contains)
                || (game.cameraManager.ActiveCamera.FRUSTUM.Contains(this.boundingSphere) == ContainmentType.Intersects))
                {
                    game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
                    game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                    // game.textureEffect.Alpha = 1f;
                    game.textureEffect.World = world;
                    game.textureEffect.Texture = texture;

                    game.textureEffect.CurrentTechnique.Passes[0].Apply();
                    game.GraphicsDevice.SetVertexBuffer(PlaneParams.vertsBuffer);
                    game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>
                        (PrimitiveType.TriangleList, PlaneParams.verts, 0, 8);
                    // DebugShapeRenderer.Draw(gameTime, game.cameraManager.ActiveCamera.View, game.cameraManager.ActiveCamera.Projection, world);


                }//
                base.Draw(gameTime);
                Vector3 projected =
                    game.GraphicsDevice.Viewport.Project(
                    Vector3.Zero, game.cameraManager.ActiveCamera.Projection,
                    game.cameraManager.ActiveCamera.View, world);





                if (rayPicked)
                {
                    game.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullNone);
                    game.spriteBatch.DrawString(game.font, "" + name, new Vector2(projected.X, projected.Y - 70f), Color.Green,
                        0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                    game.spriteBatch.DrawString(game.font, "Health " + health, new Vector2(projected.X, projected.Y - 50f), Color.Red,
                        0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0f);
                    game.spriteBatch.End();
                }
            }


        }



    }
}
