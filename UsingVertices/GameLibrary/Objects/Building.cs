﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class Building : TexturedCube
    {
        Vector3 vScale = Vector3.One;
        public BoundingBox boundingBox;
        public Vector3 mini, maxi;
        public Building(Vector3 Position,Texture2D texture,Vector3 scale):base("Building",scale,Position,texture)
        {
            mini = TexturedCubeParams.minCorner;
            maxi = TexturedCubeParams.maxCorner;
            Random rand = new Random();
            int scaleX = rand.Next(40, 70);
            int scaleY = rand.Next(80, 280);
            vScale = scale;
            Position.Y += scaleY/2;
            boundingBox = new BoundingBox(min * scale, max * scale);
        }
        public override void onCollide(Object3D obj)
        {

            if (obj is AirPlane)
            {
                AirPlane pl = (AirPlane)obj;
                pl.velocity *= -1;
            }
            base.onCollide(obj);
        }
        public override void Update(GameTime gameTime)
        {
            this.world = Matrix.Identity * Matrix.CreateScale(vScale) * Matrix.CreateTranslation(Position);
            boundingBox = new BoundingBox((mini * vScale) + Translation, (maxi * vScale) + Translation);

            if (Vector3.Distance(game.plane.POSITION, this.Position) < 300)
            {
                if (game.plane.boundingSphere.Intersects(this.boundingBox))
                {
                    game.plane.velocity *= -1;
                }
            }

           // base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            game.textureEffect.World = world;
            game.textureEffect.Texture = texture;

            game.textureEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(TexturedCubeParams.vertsBuffer);
            game.GraphicsDevice.Indices = TexturedCubeParams.indexBuffer;
            game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 24, 0, 12);
         //   base.Draw(gameTime);
        }
        
    }
}
