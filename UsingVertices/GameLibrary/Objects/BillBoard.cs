﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using UsingVertices;
using Microsoft.Xna.Framework.Graphics;

namespace GameLibrary
{
    public class BillBoard 
    {
        public static Main game;
        protected Matrix billBoard;
        protected Object3D targetObject;
        protected Vector3 scale;
        protected ObjectiveObject targetObjective;
        protected Texture2D texture;
        float bobNumber = 10f;
        float bobAmount = 0.5f;
        protected Vector3 offset;
        public bool doBob = true;

        public Matrix BILLBOARD
        {
            get { return billBoard; }
            set { billBoard = value; }
        }

        public BillBoard(Object3D targetObject,Texture2D texture,Vector3 offset,Vector3 scale)
        {
            this.targetObject = targetObject;
            this.texture = texture;
            this.offset = offset;
            this.scale = scale;
            billBoard = Matrix.CreateBillboard(targetObject.POSITION + offset, game.cameraManager.ActiveCamera.Position, game.cameraManager.ActiveCamera.Up,
                game.cameraManager.ActiveCamera.Look);

            game.billBoardManager.Add(this);
        }
        public BillBoard(ObjectiveObject targetObjective, Texture2D texture, Vector3 offset,Vector3 scale)
        {
            this.targetObjective = targetObjective;
            this.texture = texture;
            this.offset = offset;
            this.scale = scale;
            //billBoard = Matrix.CreateBillboard(targetObjective.POSITION + offset, game.cameraManager.ActiveCamera.Position, game.cameraManager.ActiveCamera.Up,
            //    game.cameraManager.ActiveCamera.Look);

            game.billBoardManager.Add(this);
        }

        public  void Draw(GameTime gameTime)
        {

            game.textureEffect.World = billBoard;
            game.textureEffect.Texture = texture;
            game.textureEffect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.SetVertexBuffer(ObjectiveArrowParams.vertsBuffer);
            // game.GraphicsDevice.Indices = ObjectiveArrowParams.indexBuffer;
            game.GraphicsDevice.DrawUserPrimitives<VertexPositionColorTexture>
                (PrimitiveType.TriangleStrip, ObjectiveArrowParams.verts, 0, 1);
           // base.Draw(gameTime);
        }
        public  void Update(GameTime gameTime)
        {
            if (doBob)
            {
                bobNumber += bobAmount;

                if (bobNumber >= 25f)
                {
                    bobAmount *= -1;
                }
                if (bobNumber <= 10f)
                {
                    bobAmount *= -1;
                }
            }
            if (scale.Length() < 6f)
            {
                scale = Vector3.One * 6;
            }
            if (targetObject != null)
            {
                billBoard = Matrix.Identity * Matrix.CreateScale(scale) * 
                    Matrix.CreateBillboard((targetObject.POSITION + new Vector3(offset.X,offset.Y + bobNumber,offset.Z)),
                    game.cameraManager.ActiveCamera.Position, game.cameraManager.ActiveCamera.Up,
                 game.cameraManager.ActiveCamera.Look);
            }
            if (targetObjective != null && targetObjective.active)
            {
                billBoard = Matrix.Identity * Matrix.CreateScale(scale) * 
                    Matrix.CreateBillboard((targetObjective.POSITION + new Vector3(offset.X, offset.Y + bobNumber, offset.Z)),
                    game.cameraManager.ActiveCamera.Position, game.cameraManager.ActiveCamera.Up,
               game.cameraManager.ActiveCamera.Look);
            }
          //  base.Update(gameTime);
        }
    }
}
